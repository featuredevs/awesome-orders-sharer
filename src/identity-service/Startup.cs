using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Database;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Services;
using IdentityModel;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.AspNetIdentity;
using IdentityServer4.EntityFramework.DbContexts;
using MassTransit;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace AwesomeOrdersSharer.IdentityService.Presentation
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<IndentityServiceDbContext>(options => options.AddNpgsqlDb(_configuration));

            services.AddIdentityServer()
                .AddConfigurationStore(options => options.ConfigureDbContext = builder => builder.AddNpgsqlDb(_configuration))
                .AddSigningCredential(new X509Certificate2(Path.Combine(Directory.GetCurrentDirectory(), "otus-aos-cert.pfx"),
                    "otus-aos-cert"))
                .AddOperationalStore(options => options.ConfigureDbContext = builder => builder.AddNpgsqlDb(_configuration))
                /*.AddEntityFrameworkStores<ApplicationDbContext>()*/
                .AddProfileService<ProfileService<ApplicationUser>>()
                .AddCorsPolicyService<AllowAnyCorsPolicyService>();

            services.AddIdentity<ApplicationUser, IdentityRole>(
                    options =>
                    {
                        //password
                        options.Password.RequireDigit = false;
                        options.Password.RequiredLength = 6;
                        options.Password.RequireNonAlphanumeric = false;
                        options.Password.RequireUppercase = false;
                        options.Password.RequireLowercase = false;
                        options.Password.RequiredUniqueChars = 2;

                        //lockout
                        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                        options.Lockout.MaxFailedAccessAttempts = 5;
                        options.Lockout.AllowedForNewUsers = true;

                        options.User.RequireUniqueEmail = true;
                    })
                .AddEntityFrameworkStores<IndentityServiceDbContext>()
                .AddUserManager<UserManager>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            services.AddCors(
                options => options.AddPolicy("IdentityServerPolicy",
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()));

            services.AddScoped<IVerificationCodeStore, VerificationCodeStore<IndentityServiceDbContext>>();

            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserIdClaimType = JwtClaimTypes.Subject;
                options.ClaimsIdentity.UserNameClaimType = JwtClaimTypes.Name;
                options.ClaimsIdentity.RoleClaimType = JwtClaimTypes.Role;
            });

            services.Configure<SecurityStampValidatorOptions>(opts =>
            {
                opts.OnRefreshingPrincipal = SecurityStampValidatorCallback.UpdatePrincipal;
            });

            services.Configure<CookieAuthenticationOptions>(IdentityConstants.ApplicationScheme,
                cookie => { cookie.Cookie.SameSite = SameSiteMode.None; });

            services.AddAuthentication()
                .AddIdentityServerAuthentication(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.Authority = _configuration.GetSection("Identity")["Authority"];
                    options.RequireHttpsMetadata = true;
                    options.ApiName = _configuration.GetSection("Identity")["Audience"];
                    options.SupportedTokens = SupportedTokens.Jwt;
                    options.JwtValidationClockSkew = TimeSpan.Zero;
                })
                .AddGoogle(options =>
                {
                    options.ClientId = _configuration.GetSection("GoogleAccount")["ClientId"];
                    options.ClientSecret = _configuration.GetSection("GoogleAccount")["ClientSecret"];
                    options.CallbackPath = _configuration.GetSection("GoogleAccount")["Callback"];
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("IdentityScope", policy =>
                    policy.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser()
                        .RequireScope("identity-api"));
            });

            services.AddScoped<IMessageBrokerService, MessageBrokerService>();

            services.AddMassTransit(config =>
            {
                config.UsingRabbitMq((context, factoryConfig) =>
                {
                    factoryConfig.Host(new Uri(_configuration["RabbitMq:Uri"]));
                    factoryConfig.ConfigureEndpoints(context);
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();

            app.UseRouting();
            app.UseStaticFiles();

            app.UseIdentityServer();
            app.UseCors();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute());

            app.InitializeDb();
        }
    }
}