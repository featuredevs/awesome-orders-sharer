﻿using System;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Models
{
    public class UserUpdateEventModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset RegistrationDate { get; set; }
        public bool IsLocked { get; set; }
        public bool IsEmailConfirmed { get; set; }
    }
}