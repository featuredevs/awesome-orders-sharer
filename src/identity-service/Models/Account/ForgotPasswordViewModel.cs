﻿using Microsoft.AspNetCore.Mvc;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Models.Account
{
    public class ForgotPasswordViewModel : ReturnUrlModel
    {
        [HiddenInput]
        public string UserId { get; set; }

        public string Email { get; set; }
    }
}