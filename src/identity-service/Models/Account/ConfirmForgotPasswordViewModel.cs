﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Models.Account
{
    public class ConfirmForgotPasswordViewModel : ReturnUrlModel
    {
        [HiddenInput]
        public string UserId { get; set; }

        public string Email { get; set; }

        [Required]
        public string Code { get; set; }
    }
}