﻿using System.ComponentModel.DataAnnotations;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Models.Account
{
    public class RegisterViewModel : ReturnUrlModel
    {
        [Required]
        [Display(Name = "First Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long and must match Password.",
            MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string PasswordConfirmation { get; set; }

        [Required]
        // Todo make proper validation
        [Range(19000101, 20200101, ErrorMessage = "Date of birth should be specified in format yyyyMMdd in range [19000101, 20200101]")]
        [Display(Name = "Date of Birth")]
        public int DateOfBirth { get; set; }
    }
}