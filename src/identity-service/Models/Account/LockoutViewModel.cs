﻿using System;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Models.Account
{
    public class LockoutViewModel : ReturnUrlModel
    {
        public TimeSpan LockoutOffset { get; set; }
    }
}