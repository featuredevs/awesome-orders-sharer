﻿namespace AwesomeOrdersSharer.IdentityService.Presentation.Models
{
    public class ReturnUrlModel
    {
        public string ReturnUrl { get; set; }
    }
}