﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Services;
using AwesomeOrdersSharer.IdentityService.Presentation.Models.Account;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Controllers
{
    [Authorize]
    [SecurityHeaders]
    public class AccountController : Controller
    {
        private readonly AccountService _accountService;
        private readonly ILogger<AccountController> _logger;
        private readonly IMessageBrokerService _messageBrokerService;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly UserManager _userManager;

        public AccountController(
            UserManager userManager,
            SignInManager<ApplicationUser> signInManager,
            IIdentityServerInteractionService interaction,
            IHttpContextAccessor httpContext,
            ILogger<AccountController> logger, IMessageBrokerService messageBrokerService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _messageBrokerService = messageBrokerService;

            _accountService = new AccountService(interaction, httpContext);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            var vm = await _accountService.BuildLoginViewModelAsync(returnUrl);
            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginInputModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(await _accountService.BuildLoginViewModelAsync(model));
            }

            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return View(await _accountService.BuildLoginViewModelAsync(model));
            }

            if (string.IsNullOrWhiteSpace(user.PasswordHash))
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt. Try to login by social network account.");
                return View(await _accountService.BuildLoginViewModelAsync(model));
            }

            if (!user.EmailConfirmed && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                return await SendEmailVerificationCodeAsync(user, model.ReturnUrl);
            }

            var result = await _signInManager.PasswordSignInAsync(user, model.Password, true, false);
            if (result.Succeeded)
            {
                return Redirect(model.ReturnUrl);
            }

            if (result.IsLockedOut)
            {
                return RedirectToAction("LockedOut", "Error");
            }

            await _userManager.AccessFailedAsync(user);
            ModelState.AddModelError(string.Empty, "Invalid login attempt.");
            return View(await _accountService.BuildLoginViewModelAsync(model));
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new {returnUrl});
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                return RedirectToAction("Login");
            }

            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction("Login");
            }

            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, false, true);
            if (result.Succeeded)
            {
                return Redirect(returnUrl);
            }

            if (result.IsLockedOut)
            {
                AddErrors(IdentityResult.Failed(new IdentityError
                {
                    Code = "403",
                    Description = "Account is locked out!"
                }));
                return RedirectToAction("Login");
            }

            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            var name = info.Principal.FindFirstValue(ClaimTypes.Name);
            var dateOfBirth = 0;
            if (DateTime.TryParse(info.Principal.FindFirstValue(ClaimTypes.DateOfBirth), out var dateOfBirthValue))
            {
                dateOfBirth = dateOfBirthValue.Year * 1000 + dateOfBirthValue.Month * 100 + dateOfBirthValue.Day;
            }

            var nameParts = name.Split(" ");
            var user = new ApplicationUser
            {
                UserName = email,
                Email = email,
                FirstName = nameParts[0],
                LastName = nameParts.Length > 1 ? string.Join(" ", nameParts.Skip(1)) : string.Empty,
                EmailConfirmed = true,
                DateOfBirth = dateOfBirth
            };
            var creationResult = await _userManager.CreateAsync(user);
            if (creationResult.Succeeded)
            {
                creationResult = await _userManager.AddLoginAsync(user, info);
                if (creationResult.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    return Redirect(returnUrl);
                }
            }

            AddErrors(creationResult);
            return View("Login", await _accountService.BuildLoginViewModelAsync(returnUrl));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (await _userManager.FindByEmailAsync(model.Email) != null)
            {
                ModelState.AddModelError(nameof(RegisterViewModel.Email), "Email already taken");
                return View(model);
            }

            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                RegistrationDate = DateTimeOffset.UtcNow,
                DateOfBirth = model.DateOfBirth,
                LockoutEnabled = true
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                try
                {
                    user = await _userManager.FindByEmailAsync(user.Email);
                    return await SendEmailVerificationCodeAsync(user, returnUrl);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Failed to send email verification code");
                    var userToDelete = await _userManager.FindByEmailAsync(model.Email);
                    await _userManager.DeleteAsync(userToDelete);
                    ModelState.AddModelError(nameof(RegisterViewModel.Email), "Failed to send email verification code");
                    return View(model);
                }
            }

            AddErrors(result);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(ConfirmEmailViewModel model)
        {
            if (model.UserId == null || model.ReturnUrl == null)
            {
                return RedirectToAction("Error", "Error");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null || user.EmailConfirmed)
            {
                return RedirectToAction("Error", "Error");
            }

            var result = await _userManager.ConfirmEmailAsync(user, model.Code);
            if (result.Succeeded)
            {
                await _messageBrokerService.PublishAsync(user);
                await _signInManager.SignInAsync(user, true);
                return Redirect(model.ReturnUrl);
            }

            AddErrors(result);
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Email) || model.ReturnUrl == null)
            {
                return RedirectToAction("Error", "Error");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return RedirectToAction("Error", "Error");
            }

            return await SendForgotPasswordVerificationCodeAsync(user, model.ReturnUrl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public IActionResult ConfirmForgotPassword(ConfirmForgotPasswordViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.UserId) || model.ReturnUrl == null)
            {
                return RedirectToAction("Error", "Error");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            return View("NewPassword", new ResetPasswordViewModel {ReturnUrl = model.ReturnUrl, UserId = model.UserId, Code = model.Code});
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ResendEmailVerificationCode(string userId, string returnUrl)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null || user.EmailConfirmed)
            {
                return RedirectToAction("Error", "Error");
            }

            return await SendEmailVerificationCodeAsync(user, returnUrl);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ResendForgotPasswordVerificationCode(string userId, string returnUrl)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return RedirectToAction("Error", "Error");
            }

            return await SendForgotPasswordVerificationCodeAsync(user, returnUrl);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Logout(string logoutId)
        {
            var vm = await _accountService.BuildLoggedOutViewModelAsync(logoutId);
            await _signInManager.SignOutAsync();
            await HttpContext.SignOutAsync();
            if (vm.TriggerExternalSignout)
            {
                var url = Url.Action("Logout", new {logoutId = vm.LogoutId});
                return SignOut(new AuthenticationProperties {RedirectUri = url}, vm.ExternalAuthenticationScheme);
            }

            return Redirect(vm.PostLogoutRedirectUri);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Error");
            }

            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                return View("ForgotPassword");
            }

            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("Login", new {model.ReturnUrl});
            }

            AddErrors(result);
            return View("NewPassword");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors.Where(x => x.Code != "DuplicateUserName"))
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private async Task<IActionResult> SendEmailVerificationCodeAsync(ApplicationUser user, string returnUrl)
        {
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            await _messageBrokerService.PublishAsync(new VerificationCodeNotificationModel
            {
                UserId = user.Id,
                Code = code
            });

            return View("ConfirmEmail", new ConfirmEmailViewModel
            {
                ReturnUrl = returnUrl,
                UserId = user.Id,
                Email = user.Email
            });
        }

        private async Task<IActionResult> SendForgotPasswordVerificationCodeAsync(ApplicationUser user, string returnUrl)
        {
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);

            await _messageBrokerService.PublishAsync(new VerificationCodeNotificationModel
            {
                UserId = user.Id,
                Code = code
            });

            return View("ConfirmForgotPassword", new ConfirmForgotPasswordViewModel
            {
                ReturnUrl = returnUrl,
                UserId = user.Id,
                Email = user.Email
            });
        }
    }
}