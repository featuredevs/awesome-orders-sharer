﻿using System.Threading.Tasks;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Controllers
{
    public class ErrorController : Controller
    {
        private readonly IIdentityServerInteractionService _interactionService;

        public ErrorController(IIdentityServerInteractionService interactionService)
        {
            _interactionService = interactionService;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Error(string errorId)
        {
            var message = await _interactionService.GetErrorContextAsync(errorId);
            if (message != null)
            {
                return View(message.Error);
            }

            return View();
        }

        [AllowAnonymous]
        public IActionResult ErrorMessage(string message)
        {
            return View("Error", message);
        }

        [AllowAnonymous]
        public IActionResult LockedOut()
        {
            return View("AccountLockedOut");
        }

        [AllowAnonymous]
        public IActionResult ErrorStatus(int id)
        {
            if (id == 404)
            {
                return View("PageNotFound");
            }

            return View("Error");
        }
    }
}