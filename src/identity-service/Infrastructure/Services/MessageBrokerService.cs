﻿using System.Threading;
using System.Threading.Tasks;
using MassTransit;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Services
{
    public interface IMessageBrokerService
    {
        Task PublishAsync<TMessage>(TMessage message, CancellationToken cancellationToken = default) where TMessage : class;
    }

    public class MessageBrokerService : IMessageBrokerService
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public MessageBrokerService(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task PublishAsync<TMessage>(TMessage message, CancellationToken cancellationToken = default) where TMessage : class
        {
            await _publishEndpoint.Publish(message, cancellationToken);
        }
    }
}