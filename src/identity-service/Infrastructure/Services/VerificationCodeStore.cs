﻿using System.Threading.Tasks;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Services
{
    public interface IVerificationCodeStore
    {
        Task<VerificationCode> GetCodeAsync(string userId);
        Task AddCodeAsync(VerificationCode code);
        Task UpdateCodeAsync(VerificationCode code);
        Task RemoveCodeAsync(string userId);
    }

    public class VerificationCodeStore<TContext> : IVerificationCodeStore
        where TContext : DbContext
    {
        private readonly TContext _context;

        public VerificationCodeStore(TContext context)
        {
            _context = context;
        }

        private DbSet<VerificationCode> CodesSet => _context.Set<VerificationCode>();

        public async Task<VerificationCode> GetCodeAsync(string userId)
        {
            return await CodesSet.FirstOrDefaultAsync(x => x.UserId == userId);
        }

        public async Task AddCodeAsync(VerificationCode code)
        {
            await CodesSet.AddAsync(code);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCodeAsync(VerificationCode code)
        {
            _context.Update(code);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveCodeAsync(string userId)
        {
            var code = await GetCodeAsync(userId);
            if (code == null)
            {
                return;
            }

            CodesSet.Remove(code);
            await _context.SaveChangesAsync();
        }
    }
}