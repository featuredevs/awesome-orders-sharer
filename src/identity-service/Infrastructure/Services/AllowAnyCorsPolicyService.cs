﻿using IdentityServer4.Services;
using Microsoft.Extensions.Logging;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Services
{
    public class AllowAnyCorsPolicyService : DefaultCorsPolicyService
    {
        public AllowAnyCorsPolicyService(ILogger<AllowAnyCorsPolicyService> logger) : base(logger)
        {
            AllowAll = true;
        }
    }
}