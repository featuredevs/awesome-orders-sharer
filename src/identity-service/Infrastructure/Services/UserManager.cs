﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Services
{
    public class UserManager : UserManager<ApplicationUser>
    {
        private readonly IMessageBrokerService _messageBrokerService;
        private readonly IUserStore<ApplicationUser> _userStore;
        private readonly IVerificationCodeStore _verificationCodeStore;

        public UserManager(IUserStore<ApplicationUser> userStore, IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<ApplicationUser> passwordHasher,
            IEnumerable<IUserValidator<ApplicationUser>> userValidators,
            IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger,
            IVerificationCodeStore verificationCodeStore, IMessageBrokerService messageBrokerService) :
            base(userStore, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors,
                services, logger)
        {
            _userStore = userStore;
            _verificationCodeStore = verificationCodeStore;
            _messageBrokerService = messageBrokerService;
        }

        public override async Task<IdentityResult> CreateAsync(ApplicationUser user)
        {
            var result = await base.CreateAsync(user);

            if (result.Succeeded)
            {
                await NotifyAboutChangeAsync(user);
            }

            return result;
        }

        public override async Task<IdentityResult> CreateAsync(ApplicationUser user, string password)
        {
            var result = await base.CreateAsync(user, password);

            if (result.Succeeded)
            {
                await NotifyAboutChangeAsync(user);
            }

            return result;
        }

        public override async Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {
            var result = await base.UpdateAsync(user);

            if (result.Succeeded)
            {
                await NotifyAboutChangeAsync(user);
            }

            return result;
        }

        public override async Task<string> GenerateEmailConfirmationTokenAsync(ApplicationUser user)
        {
            return await GenerateVerificationCode(user);
        }

        public override async Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user)
        {
            return await GenerateVerificationCode(user);
        }

        private async Task<string> GenerateVerificationCode(ApplicationUser user)
        {
            await _verificationCodeStore.RemoveCodeAsync(user.Id);
            var code = GetRandomCode(4);
            await _verificationCodeStore.AddCodeAsync(new VerificationCode
            {
                AttemptCount = 0,
                Code = code,
                Expiration = DateTime.UtcNow.AddMinutes(15),
                UserId = user.Id
            });

            return code;
        }

        public override async Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string token)
        {
            var codeModel = await _verificationCodeStore.GetCodeAsync(user.Id);
            var verificationError = await VerifyCodeAsync(token, codeModel);
            if (verificationError != null)
            {
                return verificationError;
            }

            if (!(_userStore is IUserEmailStore<ApplicationUser> emailStore))
            {
                return IdentityResult.Failed(new IdentityError
                {
                    Code = "500",
                    Description = "Email store not found!"
                });
            }

            await emailStore.SetEmailConfirmedAsync(user, true, CancellationToken);
            await _userStore.UpdateAsync(user, CancellationToken);
            await _verificationCodeStore.RemoveCodeAsync(codeModel.UserId);

            user = await _userStore.FindByIdAsync(user.Id, CancellationToken);
            await NotifyAboutChangeAsync(user);

            return IdentityResult.Success;
        }

        public override async Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string token, string newPassword)
        {
            var codeModel = await _verificationCodeStore.GetCodeAsync(user.Id);
            var verificationError = await VerifyCodeAsync(token, codeModel);
            if (verificationError != null)
            {
                return verificationError;
            }

            if (!(_userStore is IUserPasswordStore<ApplicationUser> passwordStore))
            {
                return IdentityResult.Failed(new IdentityError
                {
                    Code = "500",
                    Description = "Email store not found!"
                });
            }

            await passwordStore.SetPasswordHashAsync(user, PasswordHasher.HashPassword(user, newPassword), CancellationToken);
            await _userStore.UpdateAsync(user, CancellationToken);
            await _verificationCodeStore.RemoveCodeAsync(codeModel.UserId);
            return IdentityResult.Success;
        }

        public override async Task<IdentityResult> SetLockoutEnabledAsync(ApplicationUser user, bool enabled)
        {
            var result = await (enabled ? LockUser(user) : UnlockUser(user));
            if (result.Succeeded)
            {
                await NotifyAboutChangeAsync(user);
            }

            return result;
        }

        private async Task<IdentityResult> VerifyCodeAsync(string token, VerificationCode codeModel)
        {
            if (codeModel == null || codeModel.AttemptCount > 3 ||
                codeModel.Expiration < DateTime.UtcNow)
            {
                return IdentityResult.Failed(new IdentityError
                {
                    Description = "Verification code was expired. Please request another one."
                });
            }

            if (codeModel.Code != token)
            {
                codeModel.AttemptCount++;
                await _verificationCodeStore.UpdateCodeAsync(codeModel);
                return IdentityResult.Failed(new IdentityError
                {
                    Description = "Code is not correct."
                });
            }

            return null;
        }

        private static string GetRandomCode(int codeLength)
        {
            const int stringLength = 3;
            using var random = new RNGCryptoServiceProvider();
            var data = new byte[codeLength];
            random.GetBytes(data);
            var code = string.Empty;
            for (var i = 0; i < codeLength; i++)
            {
                code += data[i].ToString("D" + stringLength)[stringLength - 1];
            }

            return code;
        }

        private async Task<IdentityResult> LockUser(ApplicationUser user)
        {
            var result = await base.SetLockoutEnabledAsync(user, true);
            if (result.Succeeded)
            {
                result = await SetLockoutEndDateAsync(user, DateTimeOffset.MaxValue);
            }

            return result;
        }

        private async Task<IdentityResult> UnlockUser(ApplicationUser user)
        {
            var result = await base.SetLockoutEnabledAsync(user, false);
            if (result.Succeeded)
            {
                result = await SetLockoutEndDateAsync(user, DateTimeOffset.MinValue);
            }

            if (result.Succeeded)
            {
                result = await ResetAccessFailedCountAsync(user);
            }

            return result;
        }

        private async Task NotifyAboutChangeAsync(ApplicationUser user)
        {
            var isLocked = await IsLockedOutAsync(user);
            try
            {
                await _messageBrokerService.PublishAsync(new UpsertUserModel
                {
                    Id = Guid.Parse(user.Id),
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    RegistrationDate = user.RegistrationDate,
                    DateOfBirth = user.DateOfBirth,
                    IsLocked = isLocked,
                    IsEmailConfirmed = user.EmailConfirmed
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"Failed to notify about user profile change {user.Email} ({user.Id})");
            }
        }
    }
}