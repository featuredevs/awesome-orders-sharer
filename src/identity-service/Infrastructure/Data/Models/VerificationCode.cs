﻿using System;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models
{
    public class VerificationCode
    {
        public string UserId { get; set; }
        public string Code { get; set; }
        public DateTimeOffset Expiration { get; set; }
        public int AttemptCount { get; set; }
    }
}