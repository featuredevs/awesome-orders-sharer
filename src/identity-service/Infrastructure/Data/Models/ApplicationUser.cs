﻿using System;
using Microsoft.AspNetCore.Identity;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset RegistrationDate { get; set; }
        public int DateOfBirth { get; set; }
    }
}