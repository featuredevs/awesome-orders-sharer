﻿namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models
{
    public class VerificationCodeNotificationModel
    {
        public string UserId { get; set; }
        public string Code { get; set; }
    }
}