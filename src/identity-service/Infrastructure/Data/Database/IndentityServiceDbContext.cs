﻿using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Database
{
    public class IndentityServiceDbContext : IdentityDbContext<ApplicationUser>
    {
        public IndentityServiceDbContext(DbContextOptions<IndentityServiceDbContext> options) : base(options) { }

        public DbSet<VerificationCode> EmailVerificationCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<VerificationCode>().HasKey(x => x.UserId);
            builder.Entity<VerificationCode>().ToTable("AspNetEmailVerificationCodes");
            builder.Entity<VerificationCode>().HasOne<ApplicationUser>().WithOne()
                .HasForeignKey<VerificationCode>(x => x.UserId);
        }
    }
}