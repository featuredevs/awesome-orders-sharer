﻿using System.Diagnostics;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Database
{
    public static class DbContextOptionsBuilderExtensions
    {
        public static void AddNpgsqlDb(this DbContextOptionsBuilder builder, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("IdentityServerConnection");
            var migrationsAssembly = typeof(IndentityServiceDbContext).GetTypeInfo().Assembly.GetName().Name;

            builder.UseNpgsql(connectionString, dbOptions =>
            {
                dbOptions.MigrationsAssembly(migrationsAssembly);
                dbOptions.EnableRetryOnFailure(3);
            });
            builder.EnableSensitiveDataLogging(Debugger.IsAttached);
            builder.EnableDetailedErrors(Debugger.IsAttached);
        }
    }
}