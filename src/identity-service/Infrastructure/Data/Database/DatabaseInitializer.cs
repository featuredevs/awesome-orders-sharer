﻿using System;
using System.Linq;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Database
{
    public static class DatabaseInitializer
    {
        public static void InitializeDb(this IApplicationBuilder app)
        {
            app.InitializeDatabaseContext<ConfigurationDbContext>();
            app.InitializeDatabaseContext<PersistedGrantDbContext>();
            app.InitializeDatabaseContext<IndentityServiceDbContext>();

            app.InitializeTokenServerConfigurationDatabase();
        }

        private static void InitializeDatabaseContext<T>(this IApplicationBuilder app) where T : DbContext
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<T>();
            context.Database.SetCommandTimeout(TimeSpan.FromMinutes(5));
            context.Database.Migrate();
        }

        private static void InitializeTokenServerConfigurationDatabase(this IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();

            var context = scope.ServiceProvider
                .GetRequiredService<ConfigurationDbContext>();

            if (!context.Clients.Any())
            {
                foreach (var client in IdentityServiceConfig.GetClients())
                {
                    context.Clients.Add(client.ToEntity());
                }

                context.SaveChanges();
            }

            if (!context.IdentityResources.Any())
            {
                foreach (var resource in IdentityServiceConfig.GetIdentityResources())
                {
                    context.IdentityResources.Add(resource.ToEntity());
                }

                context.SaveChanges();
            }

            if (!context.ApiResources.Any())
            {
                foreach (var resource in IdentityServiceConfig.GetApiResources())
                {
                    context.ApiResources.Add(resource.ToEntity());
                }

                context.SaveChanges();
            }
        }
    }
}