﻿(() => {
    const warning = document.getElementById("Warning"),
        controls = document.getElementsByClassName("code"),
        codeForm = document.getElementById("SubmitForm"),
        finishValue = document.getElementById("FinishValue");

    codeForm.addEventListener("submit", submitForm);


    let verificationCode = [],
        currentPressedEvent = null;

    for (let i = 0; i < controls.length; i++) {
        controls[i].addEventListener("focus", onFocusEvent);
        controls[i].addEventListener("keypress", onKeyPress);
        controls[i].addEventListener("keyup", onKeyDownOrUp);
        controls[i].addEventListener("keydown", onKeyDownOrUp);
        controls[i].addEventListener("paste", handlePaste);
    }

    function isNumber(value) {
        const result = /^\d+$/.test(value);
        return result;
    }

    function onFocusEvent(e) {
        // set focus alwayls on first empty input 
        if (e.target.value === "") {
            Array.from(controls).find(control => control.value === "").focus();
            return false;
        }

        // set cursor only at the end of input
        setTimeout(() => {
                e.target.setSelectionRange(1, 1);
            },
            0.1);
    }

    function onKeyPress(e) {
        // handle paste event manually in Safari
        if ((e.metaKey && e.key === "v") || (e.ctrlKey && e.key === "v"))
            handlePaste();
        else if (isNumber(e.key) === false)
            e.preventDefault();
    }

    function didPressArrowStrategy(e, currentFocusedElementIndex) {
        const isArrowLeft = e.keyCode === 37,
            isArrowRight = e.keyCode === 39,
            nextElementIndex = currentFocusedElementIndex + 1,
            previousElementIndex = currentFocusedElementIndex - 1;

        if (currentFocusedElementIndex > 0 && isArrowLeft)
            controls[previousElementIndex].focus();
        else if (nextElementIndex < controls.length && isArrowRight)
            controls[nextElementIndex].focus();

        e.preventDefault();
    }

    function didAddOrDeleteValue(e, currentFocusedElementIndex) {
        const isKeyIsDeleteOrBackspace = (e.keyCode === 8 || e.keyCode === 46),
            nextElementIndex = currentFocusedElementIndex + 1,
            previousElementIndex = currentFocusedElementIndex - 1;

        if (currentFocusedElementIndex > 0 && isKeyIsDeleteOrBackspace)
            controls[previousElementIndex].focus();
        else if (currentFocusedElementIndex === 0 && isKeyIsDeleteOrBackspace)
            controls[0].focus();
        else if (nextElementIndex < controls.length)
            controls[nextElementIndex].focus();

        e.preventDefault();
    }

    function onKeyDownOrUp(e) {
        const indexFocusedEl = [...e.target.parentElement.children].indexOf(e.target),
            currentValue = controls[indexFocusedEl].value,
            isArrowRightOrLeft = e.keyCode === 37 || e.keyCode === 39,
            isClickEnter = e.keyCode === 13;


        if (isClickEnter) {
            submitForm();
            return codeForm.submit();
        }

        // handle right/left buttons
        if (e.type === "keydown" && isArrowRightOrLeft)
            return didPressArrowStrategy(e, indexFocusedEl);

        // stop event 'keyup' before last input element are deleted
        if (e.type === "keydown")
            return currentPressedEvent = e.target;
        else if (e.type === "keyup" && currentPressedEvent === e.target)
            currentPressedEvent = null;
        else return;

        // rewrite current value
        if (currentValue && isNumber(e.key) && (e.key !== currentValue))
            controls[indexFocusedEl].value = e.key;

        return didAddOrDeleteValue(e, indexFocusedEl);
    }

    function handlePaste(e) {
        // Stop data actually being pasted into div      
        e.stopPropagation();
        e.preventDefault();

        // Get pasted data via clipboard API
        const clipboardData = e.clipboardData || window.clipboardData;
        const pastedData = clipboardData.getData("Text").trim();

        if (isNumber(pastedData) === false || pastedData.length > 4)
            return e.preventDefault();

        const splitedData = pastedData.split("");

        for (let i = 0; i < splitedData.length; i++)
            controls[i].value = splitedData[i];

        controls[controls.length - 1].focus();
    }

    function submitForm() {
        verificationCode = [];

        for (let i = 0; i < controls.length; i++)
            verificationCode.push(controls[i].value);

        finishValue.value = verificationCode.join("");

        if (finishValue.value.length !== 4) {
            warning.classList.remove("d-none");
            finishValue.value = null;
        } else warning.classList.add("d-none");
    }

})();