﻿namespace AwesomeOrdersSharer.Domain.Common.Models
{
    public enum OrderState
    {
        /// <summary>
        ///     Дефолтное значение
        /// </summary>
        Undefined = 0,

        /// <summary>
        ///     Создан
        /// </summary>
        Created = 1,

        /// <summary>
        ///     Назначен на курьера
        /// </summary>
        Assigned = 2,

        /// <summary>
        ///     Доставлен
        /// </summary>
        Delivered = 3,

        /// <summary>
        ///     Отменен пользователем
        /// </summary>
        Cancelled = 4,

        /// <summary>
        ///     Удален
        /// </summary>
        Deleted = 5
    }
}