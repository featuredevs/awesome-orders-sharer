﻿namespace AwesomeOrdersSharer.Domain.Common.Models

{
    public enum OrderItemState
    {
        /// <summary>
        ///     Дефолтное значение
        /// </summary>
        Undefined = 0,

        /// <summary>
        ///     Активный
        /// </summary>
        Active = 1,

        /// <summary>
        ///     Удален
        /// </summary>
        Deleted = 2
    }
}