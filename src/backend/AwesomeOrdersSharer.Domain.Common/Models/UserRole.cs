namespace AwesomeOrdersSharer.Domain.Common.Models
{
    public enum UserRole
    {
        Unassigned = 0,
        User = 1,
        Courier = 2,
        Manager = 3
    }
}
