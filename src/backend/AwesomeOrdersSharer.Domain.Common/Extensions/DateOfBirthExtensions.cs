﻿using System;

namespace AwesomeOrdersSharer.Domain.Common.Extensions
{
    public static class DateOfBirthExtensions
    {
        public static DateTime? ToBirthdayDate(this int? birthday)
        {
            if (!birthday.HasValue)
            {
                return null;
            }

            var value = birthday.Value;
            return new DateTime(value / 10000, value % 10000 / 100, value % 100);
        }

        public static int? ToBirthdayInt(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            var value = date.Value;
            return value.Year * 10000 + value.Month * 100 + value.Day;
        }
    }
}