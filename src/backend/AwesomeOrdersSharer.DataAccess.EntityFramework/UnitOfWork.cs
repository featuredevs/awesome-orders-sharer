﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;

        public UnitOfWork(DatabaseContext context)
        {
            _context = context;
        }

        public IQueryable<T> Get<T>() where T : class, IBaseEntity
        {
            return _context.Set<T>();
        }

        public T Create<T>(T item) where T : class, IBaseEntity
        {
            ValidateItem(item);
            return _context.Set<T>().Add(item).Entity;
        }

        public T Delete<T>(T item) where T : class, IBaseEntity
        {
            ValidateItem(item);
            return _context.Set<T>().Remove(item).Entity;
        }

        public async Task<int> CommitAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        private static void ValidateItem<T>(T item) where T : class, IBaseEntity
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
        }
    }
}