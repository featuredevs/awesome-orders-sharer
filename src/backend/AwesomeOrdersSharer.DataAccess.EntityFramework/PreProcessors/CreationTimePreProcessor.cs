﻿using System;
using System.Linq;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.PreProcessors
{
    public class CreationTimePreProcessor : IDbEntityPreProcessor
    {
        public void Process(ChangeTracker changeTracker)
        {
            foreach (var entry in changeTracker.Entries<ICreationDateTrackedEntity>()
                .Where(x => x.State == EntityState.Added).Where(x => x.Entity != null))
            {
                entry.Entity.DateCreated = DateTimeOffset.UtcNow;
            }
        }
    }
}