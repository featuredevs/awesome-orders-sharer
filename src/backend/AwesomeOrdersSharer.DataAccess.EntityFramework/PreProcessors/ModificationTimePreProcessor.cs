﻿using System;
using System.Linq;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.PreProcessors
{
    public class ModificationTimePreProcessor : IDbEntityPreProcessor
    {
        public void Process(ChangeTracker changeTracker)
        {
            foreach (var entry in changeTracker.Entries<IUpdateDateTrackedEntity>()
                .Where(x => x.State == EntityState.Modified).Where(x => x.Entity != null))
            {
                entry.Entity.DateUpdated = DateTimeOffset.UtcNow;
            }
        }
    }
}