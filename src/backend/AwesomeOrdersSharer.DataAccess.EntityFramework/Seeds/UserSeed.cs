﻿using System;
using AwesomeOrdersSharer.DataAccess.Contracts.Models;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.Seeds
{
    /// <summary>
    ///     class for seed fake data (original DbUser doesn't allow setting id)
    /// </summary>
    internal class UserSeed : DbUser
    {
        public UserSeed(Guid id, string firstName, string lastName, string email, int role, int? dateOfBirth)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Role = role;
            DateOfBirth = dateOfBirth;
        }

        public DbUser Create()
        {
            return new DbUser(FirstName, LastName, Email, Role, DateOfBirth)
            {
                Id = Id
            };
        }
    }
}