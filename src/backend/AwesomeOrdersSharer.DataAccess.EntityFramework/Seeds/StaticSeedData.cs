﻿using System;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.Seeds
{
    public static class StaticSeedData
    {
        internal static UserSeed User1 => new UserSeed(
            Guid.Parse("98a70d98-f9d6-46b5-a8d9-785ca930f839"),
            "Ivan",
            "Ivanov",
            "Ivan.Ivanov@outlook.com",
            (int) UserRole.Unassigned,
            19900123);

        internal static UserSeed User2 => new UserSeed(
            Guid.Parse("5a4e73a9-e946-4d65-8262-6573c896e74f"),
            "Petr",
            "Petrov",
            "Petr.Petrov@outlook.com",
            (int) UserRole.Unassigned,
            19900715);

        internal static OrderSeed Order1 => new OrderSeed(
            Guid.Parse("27c32201-9482-413d-bbcb-e48851f5f72e"),
            User1.Id,
            (int) OrderState.Delivered,
            "Саратов, ул. Осипова, д. 5, кв. 220",
            "Москва, ул. Снежная, д. 4, кв. 20",
            User2.Id,
            new DateTimeOffset(2020, 10, 13, 7, 0, 0, new TimeSpan()),
            new DateTimeOffset(2020, 10, 13, 10, 0, 0, new TimeSpan()),
            new DateTimeOffset(2020, 10, 13, 10, 0, 0, new TimeSpan()),
            "[]");

        internal static OrderSeed Order2 => new OrderSeed(
            Guid.Parse("bfaf123c-dfaf-4ba6-92d4-57c957d995dc"),
            User2.Id,
            (int) OrderState.Created,
            "Москва, ул. Снежная, д. 4, кв. 20",
            "Саратов, ул. Осипова, д. 5, кв. 220",
            null,
            new DateTimeOffset(2020, 10, 13, 7, 0, 0, new TimeSpan()),
            new DateTimeOffset(2020, 10, 14, 23, 0, 0, new TimeSpan()),
            null,
            "[]");

        internal static OrderItemSeed OrderItem1 => new OrderItemSeed(
            Guid.Parse("375e37db-f7e6-4dbb-8480-93d69805c212"),
            Order1.Id,
            "Книга",
            (int) OrderItemState.Active,
            Order1.DateUpdated);

        internal static OrderItemSeed OrderItem2 => new OrderItemSeed(
            Guid.Parse("dbe4d127-8c1d-43f2-b902-fe44d67d59db"),
            Order1.Id,
            "Ноутбук",
            (int) OrderItemState.Active,
            Order1.DateUpdated);

        internal static OrderItemSeed OrderItem3 => new OrderItemSeed(
            Guid.Parse("7584b068-cccf-461c-af0b-590b0bc7ce45"),
            Order2.Id,
            "Пластинка",
            (int) OrderItemState.Active,
            Order2.DateUpdated);
    }
}