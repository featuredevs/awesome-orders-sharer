﻿using System;
using AwesomeOrdersSharer.DataAccess.Contracts.Models;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.Seeds
{
    /// <summary>
    ///     class for seed fake data (original DbOrder doesn't allow setting id)
    /// </summary>
    internal class OrderSeed : DbOrder
    {
        private readonly DateTimeOffset _createdDate;
        private readonly string _history;
        private readonly DateTimeOffset _updatedDate;

        public OrderSeed(Guid id, Guid authorId, int state, string deliveryAddress, string departureAddress, Guid? performerId,
            DateTimeOffset createdDate, DateTimeOffset updatedDate, DateTimeOffset? deliveryDate, string history)
        {
            Id = id;
            State = state;
            DeliveryAddress = deliveryAddress;
            DepartureAddress = departureAddress;
            PerformerId = performerId;
            AuthorId = authorId;
            DateDelivered = deliveryDate;
            _createdDate = createdDate;
            _updatedDate = updatedDate;
            _history = history;

            DateUpdated = _updatedDate;
        }

        public DbOrder Create()
        {
            return new DbOrder(State, AuthorId, PerformerId, DeliveryAddress, DepartureAddress, DateDelivered)
            {
                Id = Id,
                DateCreated = _createdDate,
                DateUpdated = _updatedDate,
                OrderHistory = _history
            };
        }
    }
}