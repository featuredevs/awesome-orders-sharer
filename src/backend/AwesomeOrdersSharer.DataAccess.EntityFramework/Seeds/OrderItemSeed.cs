﻿using System;
using AwesomeOrdersSharer.DataAccess.Contracts.Models;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.Seeds
{
    internal class OrderItemSeed : DbOrderItem
    {
        private readonly DateTimeOffset _updatedDate;

        public OrderItemSeed(Guid id, Guid orderId, string description, int state, DateTimeOffset updatedDate)
        {
            Id = id;
            OrderId = orderId;
            Description = description;
            State = state;
            _updatedDate = updatedDate;
        }

        public DbOrderItem Create()
        {
            return new DbOrderItem(OrderId, Description, State)
            {
                Id = Id,
                DateUpdated = _updatedDate
            };
        }
    }
}