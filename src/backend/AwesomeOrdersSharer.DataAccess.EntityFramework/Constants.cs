﻿namespace AwesomeOrdersSharer.DataAccess.EntityFramework
{
    public static class Constants
    {
        public const int AddressMaxLength = 1000;
        public const int DescriptionMaxLength = 2000;
        public const int FirstNameMaxLength = 100;
        public const int LastNameMaxLength = 150;
        public const int EmailAddressMaxLength = 100;
    }
}