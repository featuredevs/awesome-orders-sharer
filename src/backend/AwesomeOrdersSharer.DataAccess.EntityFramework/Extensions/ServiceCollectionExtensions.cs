﻿using System;
using System.Diagnostics;
using System.Reflection;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;
using AwesomeOrdersSharer.DataAccess.EntityFramework.PreProcessors;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.Extensions
{
    public static class ServiceCollectionExtensions
    {
        private const string ConnectionStringName = "AwesomeOrdersSharerDatabase";

        private static readonly string MigrationsAssembly =
            typeof(DatabaseContext).GetTypeInfo().Assembly.GetName().Name;

        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString(ConnectionStringName);
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ApplicationException($"Connection string {ConnectionStringName} to database not found");
            }

            return services
                .AddDbContext<DatabaseContext>(options =>
                {
                    options.UseNpgsql(connectionString, opt =>
                    {
                        opt.MigrationsAssembly(MigrationsAssembly);
                        opt.EnableRetryOnFailure(3);
                    });

                    options.UseLazyLoadingProxies();
                    options.EnableSensitiveDataLogging(Debugger.IsAttached);
                    options.EnableDetailedErrors(Debugger.IsAttached);
                })
                .AddScoped<IUnitOfWork, UnitOfWork>()
                .AddSingleton<IDbEntityPreProcessor, CreationTimePreProcessor>()
                .AddSingleton<IDbEntityPreProcessor, ModificationTimePreProcessor>();
        }
    }
}