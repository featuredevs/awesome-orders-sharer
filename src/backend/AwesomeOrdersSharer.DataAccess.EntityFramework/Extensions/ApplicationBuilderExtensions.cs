﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void ApplyDbMigrations(this IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<DatabaseContext>();

            context.Database.SetCommandTimeout(TimeSpan.FromMinutes(5));
            context.Database.Migrate();
        }
    }
}