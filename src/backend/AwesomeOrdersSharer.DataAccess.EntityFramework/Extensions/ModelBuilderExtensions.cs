﻿using AwesomeOrdersSharer.DataAccess.Contracts.Models;
using AwesomeOrdersSharer.DataAccess.EntityFramework.Seeds;
using Microsoft.EntityFrameworkCore;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.Extensions
{
    internal static class ModelBuilderExtensions
    {
        public static void SeedData(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbUser>().HasData(
                StaticSeedData.User1.Create(),
                StaticSeedData.User2.Create()
            );

            modelBuilder.Entity<DbOrderItem>().HasData(
                StaticSeedData.OrderItem1.Create(),
                StaticSeedData.OrderItem2.Create(),
                StaticSeedData.OrderItem3.Create()
            );

            modelBuilder.Entity<DbOrder>().HasData(
                StaticSeedData.Order1.Create(),
                StaticSeedData.Order2.Create()
            );
        }
    }
}