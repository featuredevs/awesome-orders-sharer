﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;
using AwesomeOrdersSharer.DataAccess.EntityFramework.Extensions;
using AwesomeOrdersSharer.DataAccess.EntityFramework.ModelConfigurations;
using Microsoft.EntityFrameworkCore;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        private readonly IEnumerable<IDbEntityPreProcessor> _preProcessors;

        public DatabaseContext(DbContextOptions<DatabaseContext> options, IEnumerable<IDbEntityPreProcessor> preProcessors)
            : base(options)
        {
            _preProcessors = preProcessors;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DbOrderConfiguration());
            modelBuilder.ApplyConfiguration(new DbOrderItemConfiguration());
            modelBuilder.ApplyConfiguration(new DbUserConfiguration());

            modelBuilder.SeedData();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entityPreProcessor in _preProcessors)
            {
                entityPreProcessor.Process(ChangeTracker);
            }

            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}