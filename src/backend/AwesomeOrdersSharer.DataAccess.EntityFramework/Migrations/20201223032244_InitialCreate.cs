﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Users",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    LastName = table.Column<string>(maxLength: 150, nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    Role = table.Column<int>(nullable: false),
                    DateOfBirth = table.Column<int>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.Id); });

            migrationBuilder.CreateTable(
                "Orders",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Number = table.Column<long>(nullable: false),
                    State = table.Column<int>(nullable: false, defaultValue: 0),
                    OrderHistory = table.Column<string>(nullable: false, defaultValue: "[]"),
                    AuthorId = table.Column<Guid>(nullable: false),
                    PerformerId = table.Column<Guid>(nullable: true),
                    DeliveryAddress = table.Column<string>(maxLength: 1000, nullable: false),
                    DepartureAddress = table.Column<string>(maxLength: 1000, nullable: false),
                    DateDelivered = table.Column<DateTimeOffset>(nullable: true),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateUpdated = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        "FK_Orders_Users_AuthorId",
                        x => x.AuthorId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_Orders_Users_PerformerId",
                        x => x.PerformerId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "OrderItems",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OrderId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(maxLength: 2000, nullable: false),
                    State = table.Column<int>(nullable: false, defaultValue: 0),
                    DateUpdated = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItems", x => x.Id);
                    table.ForeignKey(
                        "FK_OrderItems_Orders_OrderId",
                        x => x.OrderId,
                        "Orders",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                "Users",
                new[] {"Id", "DateOfBirth", "Email", "FirstName", "LastName", "Role"},
                new object[,]
                {
                    {new Guid("98a70d98-f9d6-46b5-a8d9-785ca930f839"), 19900123, "Ivan.Ivanov@outlook.com", "Ivan", "Ivanov", 0},
                    {new Guid("5a4e73a9-e946-4d65-8262-6573c896e74f"), 19900715, "Petr.Petrov@outlook.com", "Petr", "Petrov", 0}
                });

            migrationBuilder.InsertData(
                "Orders",
                new[]
                {
                    "Id", "AuthorId", "DateCreated", "DateDelivered", "DateUpdated", "DeliveryAddress", "DepartureAddress", "Number",
                    "OrderHistory", "PerformerId", "State"
                },
                new object[,]
                {
                    {
                        new Guid("27c32201-9482-413d-bbcb-e48851f5f72e"), new Guid("98a70d98-f9d6-46b5-a8d9-785ca930f839"),
                        new DateTimeOffset(new DateTime(2020, 10, 13, 7, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                        new DateTimeOffset(new DateTime(2020, 10, 13, 10, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                        new DateTimeOffset(new DateTime(2020, 10, 13, 10, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                        "Саратов, ул. Осипова, д. 5, кв. 220", "Москва, ул. Снежная, д. 4, кв. 20", 0L, "[]",
                        new Guid("5a4e73a9-e946-4d65-8262-6573c896e74f"), 3
                    },
                    {
                        new Guid("bfaf123c-dfaf-4ba6-92d4-57c957d995dc"), new Guid("5a4e73a9-e946-4d65-8262-6573c896e74f"),
                        new DateTimeOffset(new DateTime(2020, 10, 13, 7, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                        null,
                        new DateTimeOffset(new DateTime(2020, 10, 14, 23, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                        "Москва, ул. Снежная, д. 4, кв. 20", "Саратов, ул. Осипова, д. 5, кв. 220", 0L, "[]", null, 1
                    }
                });

            migrationBuilder.InsertData(
                "OrderItems",
                new[] {"Id", "DateUpdated", "Description", "OrderId", "State"},
                new object[,]
                {
                    {
                        new Guid("375e37db-f7e6-4dbb-8480-93d69805c212"),
                        new DateTimeOffset(new DateTime(2020, 10, 13, 10, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                        "Книга", new Guid("27c32201-9482-413d-bbcb-e48851f5f72e"), 1
                    },
                    {
                        new Guid("dbe4d127-8c1d-43f2-b902-fe44d67d59db"),
                        new DateTimeOffset(new DateTime(2020, 10, 13, 10, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                        "Ноутбук", new Guid("27c32201-9482-413d-bbcb-e48851f5f72e"), 1
                    },
                    {
                        new Guid("7584b068-cccf-461c-af0b-590b0bc7ce45"),
                        new DateTimeOffset(new DateTime(2020, 10, 14, 23, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                        "Пластинка", new Guid("bfaf123c-dfaf-4ba6-92d4-57c957d995dc"), 1
                    }
                });

            migrationBuilder.CreateIndex(
                "IX_OrderItems_OrderId",
                "OrderItems",
                "OrderId");

            migrationBuilder.CreateIndex(
                "IX_Orders_AuthorId",
                "Orders",
                "AuthorId");

            migrationBuilder.CreateIndex(
                "IX_Orders_PerformerId",
                "Orders",
                "PerformerId");

            migrationBuilder.CreateIndex(
                "IX_Users_Email",
                "Users",
                "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "OrderItems");

            migrationBuilder.DropTable(
                "Orders");

            migrationBuilder.DropTable(
                "Users");
        }
    }
}