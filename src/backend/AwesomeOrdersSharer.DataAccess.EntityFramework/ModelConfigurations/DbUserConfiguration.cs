﻿using AwesomeOrdersSharer.DataAccess.Contracts.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.ModelConfigurations
{
    public class DbUserConfiguration : IEntityTypeConfiguration<DbUser>
    {
        public void Configure(EntityTypeBuilder<DbUser> builder)
        {
            builder.ToTable("Users");

            builder.Property(x => x.FirstName)
                .HasMaxLength(Constants.FirstNameMaxLength)
                .IsRequired();

            builder.Property(x => x.LastName)
                .HasMaxLength(Constants.LastNameMaxLength)
                .IsRequired();

            builder.Property(x => x.Email)
                .HasMaxLength(Constants.EmailAddressMaxLength)
                .IsRequired();

            builder.HasIndex(x => x.Email)
                .IsUnique();
        }
    }
}