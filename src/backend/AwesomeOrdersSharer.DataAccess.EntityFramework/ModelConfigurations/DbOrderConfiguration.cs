﻿using AwesomeOrdersSharer.DataAccess.Contracts.Models;
using AwesomeOrdersSharer.Domain.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.ModelConfigurations
{
    public class DbOrderConfiguration : IEntityTypeConfiguration<DbOrder>
    {
        public void Configure(EntityTypeBuilder<DbOrder> builder)
        {
            builder.ToTable("Orders");

            builder.HasQueryFilter(o => o.State != (int) OrderState.Deleted);

            builder.HasOne(x => x.Author)
                .WithMany()
                .HasForeignKey(x => x.AuthorId);

            builder.HasOne(x => x.Performer)
                .WithMany()
                .HasForeignKey(x => x.PerformerId);

            builder.Property(x => x.OrderHistory)
                .IsRequired()
                .HasDefaultValue("[]");

            builder.Property(x => x.DeliveryAddress)
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(x => x.DepartureAddress)
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(x => x.State)
                .IsRequired()
                .HasDefaultValue((int) OrderState.Undefined);
        }
    }
}