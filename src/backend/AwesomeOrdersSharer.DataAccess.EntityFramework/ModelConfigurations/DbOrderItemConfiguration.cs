﻿using AwesomeOrdersSharer.DataAccess.Contracts.Models;
using AwesomeOrdersSharer.Domain.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AwesomeOrdersSharer.DataAccess.EntityFramework.ModelConfigurations
{
    public class DbOrderItemConfiguration : IEntityTypeConfiguration<DbOrderItem>
    {
        public void Configure(EntityTypeBuilder<DbOrderItem> builder)
        {
            builder.ToTable("OrderItems");

            builder.HasQueryFilter(o => o.State != (int) OrderState.Deleted);

            builder.HasOne(x => x.Order)
                .WithMany(x => x.Items)
                .HasForeignKey(x => x.OrderId);

            builder.Property(x => x.Description)
                .HasMaxLength(Constants.DescriptionMaxLength)
                .IsRequired();

            builder.Property(x => x.State)
                .IsRequired()
                .HasDefaultValue((int) OrderState.Undefined);
        }
    }
}