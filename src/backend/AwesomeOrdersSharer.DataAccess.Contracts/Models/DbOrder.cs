﻿using System;
using System.Collections.Generic;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;

namespace AwesomeOrdersSharer.DataAccess.Contracts.Models
{
    public class DbOrder : BaseGuidEntity, ICreationDateTrackedEntity, IUpdateDateTrackedEntity
    {
        protected DbOrder() { }

        public DbOrder(int state, Guid authorId, Guid? performerId, string deliveryAddress, string departureAddress,
            DateTimeOffset? dateDelivered)
        {
            State = state;
            AuthorId = authorId;
            PerformerId = performerId;
            DeliveryAddress = deliveryAddress;
            DepartureAddress = departureAddress;
            DateDelivered = dateDelivered;
        }

        public long Number { get; set; }

        public int State { get; set; }

        public string OrderHistory { get; set; }

        public Guid AuthorId { get; protected set; }

        public virtual DbUser Author { get; protected set; }

        public Guid? PerformerId { get; protected set; }

        public virtual DbUser Performer { get; protected set; }

        public virtual ICollection<DbOrderItem> Items { get; protected set; } = new List<DbOrderItem>();

        public string DeliveryAddress { get; protected set; }

        public string DepartureAddress { get; protected set; }

        public DateTimeOffset? DateDelivered { get; protected set; }

        public DateTimeOffset DateCreated { get; set; }

        public DateTimeOffset DateUpdated { get; set; }

        public void Update(int? state, Guid? performerId, string deliveryAddress, string departureAddress, DateTimeOffset? dateDelivered)
        {
            PerformerId = performerId ?? PerformerId;
            State = state ?? State;
            DeliveryAddress = deliveryAddress ?? DeliveryAddress;
            DepartureAddress = departureAddress ?? DepartureAddress;
            DateDelivered = dateDelivered ?? DateDelivered;
        }
    }
}