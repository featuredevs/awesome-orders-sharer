﻿using System;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;

namespace AwesomeOrdersSharer.DataAccess.Contracts.Models
{
    public class DbOrderItem : BaseGuidEntity, IUpdateDateTrackedEntity
    {
        protected DbOrderItem() { }

        public DbOrderItem(Guid orderId, string description, int state)
        {
            OrderId = orderId;
            Description = description;
            State = state;
        }

        public Guid OrderId { get; set; }

        public virtual DbOrder Order { get; protected set; }

        public string Description { get; protected set; }

        public int State { get; set; }

        public DateTimeOffset DateUpdated { get; set; }

        public void Update(string description)
        {
            Description = description ?? Description;
        }
    }
}