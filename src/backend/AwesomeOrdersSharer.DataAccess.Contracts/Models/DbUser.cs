﻿using System;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;
using AwesomeOrdersSharer.Domain.Common.Extensions;

namespace AwesomeOrdersSharer.DataAccess.Contracts.Models
{
    public class DbUser : BaseGuidEntity
    {
        protected DbUser() { }

        public DbUser(string firstName, string lastName, string email, int role, int? dateOfBirth)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Role = role;
            DateOfBirth = dateOfBirth;
        }

        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public string Email { get; protected set; }
        public int Role { get; protected set; }
        public int? DateOfBirth { get; protected set; }

        public void Update(string firstName, string lastName, string email, DateTime? dateOfBirth)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            DateOfBirth = dateOfBirth.ToBirthdayInt();
        }
    }
}