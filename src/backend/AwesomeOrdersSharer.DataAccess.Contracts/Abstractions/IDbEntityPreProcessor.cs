﻿using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AwesomeOrdersSharer.DataAccess.Contracts.Abstractions
{
    public interface IDbEntityPreProcessor
    {
        void Process(ChangeTracker changeTracker);
    }
}