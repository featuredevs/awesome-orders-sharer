﻿using System;

namespace AwesomeOrdersSharer.DataAccess.Contracts.Abstractions
{
    public interface IUpdateDateTrackedEntity
    {
        DateTimeOffset DateUpdated { get; set; }
    }
}