﻿using System;

namespace AwesomeOrdersSharer.DataAccess.Contracts.Abstractions
{
    public interface ICreationDateTrackedEntity
    {
        DateTimeOffset DateCreated { get; set; }
    }
}