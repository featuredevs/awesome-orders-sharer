using AutoMapper;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions;
using AwesomeOrdersSharer.Domain.Contracts.ServicesAbstractions;
using AwesomeOrdersSharer.WebApi.Infrastructure;
using AwesomeOrdersSharer.WebApi.Models.Request;
using AwesomeOrdersSharer.WebApi.Models.Request.Order;
using AwesomeOrdersSharer.WebApi.Models.Response;
using AwesomeOrdersSharer.WebApi.Models.Response.Order;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.WebApi.Controllers
{
    [Produces(Constants.DefaultMimeType)]
    [Route("api/orders")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private const int OK = (int)HttpStatusCode.OK;
        private readonly IMapper _mapper;
        private readonly IOrderRepository _orderRepository;
        private readonly INotificationsService _notificationsService;

        public OrderController(IOrderRepository orderRepository, IMapper mapper, INotificationsService notificationsService)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
            _notificationsService = notificationsService;
        }

        /// <summary>
        ///     Поиск заказов
        /// </summary>
        [HttpPost]
        [Route("search")]
        [SwaggerResponse(OK, Type = typeof(IEnumerable<OrderResponse>))]
        public async Task<IActionResult> SearchOrders(SearchOrdersRequest request)
        {
            var paginationResult =
                await _orderRepository.SearchOrdersAsync(_mapper.Map<SearchOrdersFilter>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<PaginationResponse<OrderResponse>>(paginationResult));
        }

        /// <summary>
        ///     Получение заказа по Id
        /// </summary>
        [HttpGet]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(OrderResponse))]
        public async Task<IActionResult> GetOrder([FromRoute] ByGuidRequest model)
        {
            var order = await _orderRepository.GetOrderAsync(model.Id, HttpContext.RequestAborted);
            return Ok(_mapper.Map<OrderResponse>(order));
        }

        /// <summary>
        ///     Создание заказа
        /// </summary>
        [HttpPost]
        [SwaggerResponse(OK, Type = typeof(OrderResponse))]
        public async Task<IActionResult> CreateOrder(CreateOrderRequest request)
        {
            var order = await _orderRepository.CreateOrderAsync(_mapper.Map<CreateOrderModel>(request), HttpContext.RequestAborted);
            await _notificationsService.NotifyOrderCreated(order);
            return Ok(_mapper.Map<OrderResponse>(order));
        }

        /// <summary>
        ///     Обновление заказа
        /// </summary>
        [HttpPut]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(OrderResponse))]
        public async Task<IActionResult> UpdateOrder([FromRoute] ByGuidRequest idModel, UpdateOrderRequest request)
        {
            var order = await _orderRepository.UpdateOrderAsync(idModel.Id, _mapper.Map<UpdateOrderModel>(request),
                HttpContext.RequestAborted);
            if (request.State.HasValue)
            {
                await _notificationsService.NotifyStatusUpdate(order);
            }

            return Ok(_mapper.Map<OrderResponse>(order));
        }

        /// <summary>
        ///     Удаление заказа
        /// </summary>
        [HttpDelete]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(OrderResponse))]
        public async Task<IActionResult> DeleteOrder([FromRoute] ByGuidRequest model)
        {
            await _orderRepository.DeleteOrderAsync(model.Id, HttpContext.RequestAborted);
            return Ok();
        }

        /// <summary>
        ///     Добавление позиции заказа
        /// </summary>
        [HttpPost]
        [Route("{id:guid}/items")]
        [SwaggerResponse(OK, Type = typeof(OrderResponse))]
        public async Task<IActionResult> AddOrderItem([FromRoute] ByGuidRequest idModel, [FromBody] AddOrderItemRequest model)
        {
            var orderItem =
                await _orderRepository.AddOrderItemAsync(idModel.Id, _mapper.Map<CreateOrderItemModel>(model), HttpContext.RequestAborted);
            return Ok(_mapper.Map<OrderItemResponse>(orderItem));
        }

        /// <summary>
        ///     Обновление позиции заказа
        /// </summary>
        [HttpPut]
        [Route("{orderId:guid}/items/{itemId:guid}")]
        [SwaggerResponse(OK, Type = typeof(OrderResponse))]
        public async Task<IActionResult> UpdateOrderItem([FromRoute] ByOrderIdAndOrderItemRequest idModel,
            [FromBody] UpdateOrderItemRequest model)
        {
            var orderItem = await _orderRepository.UpdateOrderItemAsync(idModel.OrderId, idModel.ItemId,
                _mapper.Map<UpdateOrderItemModel>(model),
                HttpContext.RequestAborted);
            return Ok(_mapper.Map<OrderItemResponse>(orderItem));
        }

        /// <summary>
        ///     Удаление позиции заказа
        /// </summary>
        [HttpDelete]
        [Route("{orderId:guid}/items/{itemId:guid}")]
        [SwaggerResponse(OK, Type = typeof(OrderResponse))]
        public async Task<IActionResult> DeleteOrderItem([FromRoute] ByOrderIdAndOrderItemRequest model)
        {
            await _orderRepository.DeleteOrderItemAsync(model.OrderId, model.ItemId, HttpContext.RequestAborted);
            return Ok();
        }
    }
}