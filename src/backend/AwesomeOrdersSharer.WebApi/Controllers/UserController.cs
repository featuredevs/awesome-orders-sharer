﻿using AutoMapper;
using AwesomeOrdersSharer.Domain.Contracts.Models.User;
using AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions;
using AwesomeOrdersSharer.WebApi.Infrastructure;
using AwesomeOrdersSharer.WebApi.Models.Request;
using AwesomeOrdersSharer.WebApi.Models.Request.User;
using AwesomeOrdersSharer.WebApi.Models.Response;
using AwesomeOrdersSharer.WebApi.Models.Response.User;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.WebApi.Controllers
{
    [Produces(Constants.DefaultMimeType)]
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        /// <summary>
        ///     Получение пользователя по Id
        /// </summary>
        [HttpGet]
        [Route("{id:guid}")]
        [SwaggerResponse(200, Type = typeof(UserResponse))]
        public async Task<IActionResult> GetUser([FromRoute] ByGuidRequest model)
        {
            var user = await _userRepository.GetUserAsync(model.Id, HttpContext.RequestAborted);
            return Ok(_mapper.Map<UserResponse>(user));
        }

        /// <summary>
        ///     Search users
        /// </summary>
        [HttpPost]
        [Route("search")]
        [SwaggerResponse(200, Type = typeof(PaginationResponse<UserResponse>))]
        public async Task<IActionResult> SearchUsers([FromBody] SearchUsersRequest model)
        {
            var paginationResult =
                await _userRepository.SearchUsersAsync(_mapper.Map<SearchUsersFilter>(model), HttpContext.RequestAborted);
            return Ok(_mapper.Map<PaginationResponse<UserResponse>>(paginationResult));
        }
    }
}