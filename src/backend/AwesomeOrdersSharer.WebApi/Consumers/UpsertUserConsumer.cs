﻿using AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using MassTransit;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.WebApi.Consumers
{
    public class UpsertUserConsumer : IConsumer<UpsertUserModel>
    {
        private readonly IUserRepository _userRepository;

        public UpsertUserConsumer(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task Consume(ConsumeContext<UpsertUserModel> context)
        {
            await _userRepository.UpsertUserAsync(context.Message, context.CancellationToken);
        }
    }
}
