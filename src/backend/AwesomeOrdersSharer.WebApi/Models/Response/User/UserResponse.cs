﻿using System;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.WebApi.Models.Response.User
{
    public class UserResponse
    {
        protected UserResponse() { }

        public UserResponse(Guid id, string firstName, string lastName, string email, DateTime? dateOfBirth, UserRole role)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            DateOfBirth = dateOfBirth;
            Role = role;
        }

        public Guid Id { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public string Email { get; protected set; }
        public DateTime? DateOfBirth { get; protected set; }
        public UserRole Role { get; protected set; }
    }
}