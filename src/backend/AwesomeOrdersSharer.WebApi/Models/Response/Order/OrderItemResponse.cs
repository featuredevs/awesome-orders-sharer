﻿using System;

namespace AwesomeOrdersSharer.WebApi.Models.Response.Order
{
    public class OrderItemResponse
    {
        protected OrderItemResponse() { }

        public OrderItemResponse(Guid id, string description)
        {
            Id = id;
            Description = description;
        }

        public Guid Id { get; protected set; }

        public string Description { get; protected set; }
    }
}