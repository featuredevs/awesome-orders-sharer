﻿using System;
using System.Collections.Generic;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.WebApi.Models.Response.Order
{
    /// <summary>
    ///     Заказ
    /// </summary>
    public class OrderResponse
    {
        protected OrderResponse() { }

        public OrderResponse(Guid id, OrderState state, string history, Guid authorId, Guid? performerId, long number,
            IEnumerable<OrderItemResponse> items, DateTime? deliveryDate, string deliveryAddress, string departureAddress)
        {
            Id = id;
            State = state;
            History = history;
            AuthorId = authorId;
            PerformerId = performerId;
            Number = number;
            Items = items;
            DeliveryDate = deliveryDate;
            DeliveryAddress = deliveryAddress;
            DepartureAddress = departureAddress;
        }

        /// <summary>
        ///     Номер заказа
        /// </summary>
        public Guid Id { get; protected set; }

        /// <summary>
        ///     Статус заказа
        /// </summary>
        public OrderState State { get; protected set; }

        /// <summary>
        ///     История заказа
        /// </summary>
        public string History { get; protected set; }

        /// <summary>
        ///     Автор заказа
        /// </summary>
        public Guid AuthorId { get; protected set; }

        /// <summary>
        ///     Исполнитель заказа
        /// </summary>
        public Guid? PerformerId { get; protected set; }

        /// <summary>
        ///     Номер заказа
        /// </summary>
        public long Number { get; protected set; }

        /// <summary>
        ///     Позиции заказа
        /// </summary>
        public IEnumerable<OrderItemResponse> Items { get; protected set; }

        /// <summary>
        ///     Адрес доставки
        /// </summary>
        public string DeliveryAddress { get; protected set; }

        /// <summary>
        ///     Адрес отправления
        /// </summary>
        public string DepartureAddress { get; protected set; }

        /// <summary>
        ///     Дата доставки
        /// </summary>
        public DateTime? DeliveryDate { get; protected set; }
    }
}