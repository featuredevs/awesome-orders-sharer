using AutoMapper;
using AwesomeOrdersSharer.Domain.Contracts.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.Domain.Contracts.Models.User;
using AwesomeOrdersSharer.WebApi.Models.Response;
using AwesomeOrdersSharer.WebApi.Models.Response.Order;
using AwesomeOrdersSharer.WebApi.Models.Response.User;

namespace AwesomeOrdersSharer.WebApi.Models.MappingProfiles
{
    public class ResponseMappingProfile : Profile
    {
        public ResponseMappingProfile()
        {
            CreateMap<OrderReadModel, OrderResponse>()
                .ForMember(d => d.Items, opts => opts.MapFrom(src => src.Items))
                .ForMember(d => d.DeliveryDate, opts => opts.MapFrom(src => src.DateDelivered));
            CreateMap<OrderItemReadModel, OrderItemResponse>();
            CreateMap<UserReadModel, UserResponse>();
            CreateMap(typeof(PaginationReadModel<>), typeof(PaginationResponse<>));
        }
    }
}