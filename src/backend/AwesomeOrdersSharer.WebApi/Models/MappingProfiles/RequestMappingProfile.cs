using AutoMapper;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.Domain.Contracts.Models.User;
using AwesomeOrdersSharer.WebApi.Models.Request.Order;
using AwesomeOrdersSharer.WebApi.Models.Request.User;

namespace AwesomeOrdersSharer.WebApi.Models.MappingProfiles
{
    public class RequestMappingProfile : Profile
    {
        public RequestMappingProfile()
        {
            CreateMap<UpdateOrderRequest, UpdateOrderModel>();
            CreateMap<CreateOrderRequest, CreateOrderModel>();
            CreateMap<CreateOrderItemRequest, CreateOrderItemModel>();
            CreateMap<UpdateOrderItemRequest, UpdateOrderItemModel>();
            CreateMap<SearchOrdersRequest, SearchOrdersFilter>();
            CreateMap<SearchUsersRequest, SearchUsersFilter>();
        }
    }
}