using AutoMapper;
using AwesomeOrdersSharer.DataAccess.Contracts.Models;
using AwesomeOrdersSharer.Domain.Common.Extensions;
using AwesomeOrdersSharer.Domain.Common.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.Domain.Contracts.Models.User;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using System;

namespace AwesomeOrdersSharer.WebApi.Models.MappingProfiles
{
    public class DbMappingProfile : Profile
    {
        public DbMappingProfile()
        {
            CreateMap<DbOrder, OrderReadModel>()
                .ForMember(d => d.Items, opts => opts.MapFrom(src => src.Items))
                .ForMember(d => d.DateDelivered,
                    opts => opts.MapFrom(src => src.DateDelivered.HasValue ? new DateTime?(src.DateDelivered.Value.DateTime) : null))
                .ForMember(d => d.State, opts => opts.MapFrom(src => (OrderState)src.State))
                .ForMember(d => d.History, opts => opts.MapFrom(src => src.OrderHistory));
            CreateMap<DbOrderItem, OrderItemReadModel>();
            CreateMap<DbUser, UserReadModel>()
                .ForMember(d => d.Role, opts => opts.MapFrom(src => (UserRole)src.Role))
                .ForMember(d => d.DateOfBirth, opts => opts.MapFrom(src => src.DateOfBirth.ToBirthdayDate()));

            CreateMap<CreateOrderModel, DbOrder>()
                .ForMember(d => d.State, opts => opts.MapFrom(src => (int)OrderState.Created));
            CreateMap<UpdateOrderItemModel, DbOrderItem>();

            CreateMap<UpsertUserModel, DbUser>();
        }
    }
}