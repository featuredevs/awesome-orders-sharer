﻿using System;
using AwesomeOrdersSharer.Domain.Common.Extensions;
using AwesomeOrdersSharer.Domain.Common.Models;
using AwesomeOrdersSharer.WebApi.Infrastructure;
using FluentValidation;

namespace AwesomeOrdersSharer.WebApi.Models.Request.User
{
    public class SearchUsersRequest : LimitOffsetRequest
    {
        public string TextFilter { get; set; }
        public int? DateOfBirth { get; set; }
        public UserRole? Role { get; set; }
    }


    public class SearchUserRequestValidator : LimitOffsetRequestValidator<SearchUsersRequest>
    {
        public SearchUserRequestValidator()
        {
            When(x => !string.IsNullOrWhiteSpace(x.TextFilter), () =>
            {
                RuleFor(x => x.TextFilter)
                    .MinimumLength(Constants.TextFilterMinLength)
                    .WithMessage($@"When specified, TextFilter should be minimum of {Constants.TextFilterMinLength} characters");
            });

            When(x => x.Role.HasValue, () =>
            {
                RuleFor(x => x.Role)
                    .IsInEnum();
            });

            When(x => x.DateOfBirth.HasValue, () =>
            {
                RuleFor(x => x.DateOfBirth.ToBirthdayDate())
                    .NotNull()
                    .LessThan(p => DateTime.Today)
                    .WithMessage("Date of your birth must no more then current date");
            });
        }
    }
}