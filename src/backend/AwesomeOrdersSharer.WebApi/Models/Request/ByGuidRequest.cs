﻿using System;
using FluentValidation;

namespace AwesomeOrdersSharer.WebApi.Models.Request
{
    public class ByGuidRequest
    {
        public Guid Id { get; set; }
    }

    public class ByGuidRequestValidator : AbstractValidator<ByGuidRequest>
    {
        public ByGuidRequestValidator()
        {
            RuleFor(x => x.Id)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"Id should be specified and non-empty GUID\UUID");
        }
    }
}