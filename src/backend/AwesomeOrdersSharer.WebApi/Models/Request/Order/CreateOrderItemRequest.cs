﻿using AwesomeOrdersSharer.WebApi.Infrastructure;
using FluentValidation;

namespace AwesomeOrdersSharer.WebApi.Models.Request.Order
{
    public class CreateOrderItemRequest
    {
        /// <summary>
        ///     Описание позиции заказа
        /// </summary>
        public string Description { get; set; }
    }

    public class CreateOrderItemRequestValidator<T> : AbstractValidator<T> where T : CreateOrderItemRequest
    {
        public CreateOrderItemRequestValidator()
        {
            RuleFor(x => x.Description)
                .Must(x => !string.IsNullOrWhiteSpace(x) && x.Length <= Constants.DescriptionMaxLength)
                .WithMessage($"Order item must have a non-empty description no more than {Constants.DescriptionMaxLength} characters");
        }
    }
}