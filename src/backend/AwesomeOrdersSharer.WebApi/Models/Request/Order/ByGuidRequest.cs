﻿using System;
using FluentValidation;

namespace AwesomeOrdersSharer.WebApi.Models.Request.Order
{
    public class ByOrderIdAndOrderItemRequest
    {
        public Guid OrderId { get; set; }
        public Guid ItemId { get; set; }
    }

    public class ByOrderIdAndOrderItemRequestValidator : AbstractValidator<ByOrderIdAndOrderItemRequest>
    {
        public ByOrderIdAndOrderItemRequestValidator()
        {
            RuleFor(x => x.OrderId)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"OrderId should be specified and non-empty GUID\UUID");

            RuleFor(x => x.ItemId)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"ItemId should be specified and non-empty GUID\UUID");
        }
    }
}