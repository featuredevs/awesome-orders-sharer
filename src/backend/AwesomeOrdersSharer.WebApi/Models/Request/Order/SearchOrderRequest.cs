﻿using System;
using AwesomeOrdersSharer.Domain.Common.Models;
using FluentValidation;

namespace AwesomeOrdersSharer.WebApi.Models.Request.Order
{
    /// <summary>
    ///     Поиск заказа
    /// </summary>
    public class SearchOrdersRequest : LimitOffsetRequest
    {
        /// <summary>
        ///     Автор заказа
        /// </summary>
        public Guid? AuthorId { get; set; }

        /// <summary>
        ///     Статус заказа
        /// </summary>
        public OrderState? State { get; set; }

        /// <summary>
        ///     Исполнитель заказа
        /// </summary>
        public Guid? PerformerId { get; set; }
    }

    public class SearchOrdersRequestValidator : LimitOffsetRequestValidator<SearchOrdersRequest>
    {
        public SearchOrdersRequestValidator()
        {
            When(x => x.AuthorId.HasValue, () =>
            {
                RuleFor(x => x.AuthorId.Value)
                    .Must(x => x != Guid.Empty && x != default)
                    .WithMessage(@"AuthorId should be specified and non-empty GUID\UUID");
            });

            When(x => x.State.HasValue, () =>
            {
                RuleFor(x => x.State)
                    .IsInEnum()
                    .WithMessage($"State should be specified to one of values: {string.Join(", ", Enum.GetNames(typeof(OrderState)))}");
            });

            When(x => x.PerformerId.HasValue, () =>
            {
                RuleFor(x => x.PerformerId.Value)
                    .Must(x => x != Guid.Empty && x != default)
                    .WithMessage(@"PerformerId should be specified and non-empty GUID\UUID");
            });
        }
    }
}