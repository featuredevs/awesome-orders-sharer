﻿using System;
using System.Collections.Generic;
using AwesomeOrdersSharer.WebApi.Infrastructure;
using FluentValidation;

namespace AwesomeOrdersSharer.WebApi.Models.Request.Order
{
    /// <summary>
    ///     Создание заказа
    /// </summary>
    public class CreateOrderRequest
    {
        /// <summary>
        ///     Автор заказа
        /// </summary>
        public Guid AuthorId { get; set; }

        /// <summary>
        ///     Позиции заказа
        /// </summary>
        public IEnumerable<CreateOrderItemRequest> Items { get; set; }

        /// <summary>
        ///     Адрес доставки
        /// </summary>
        public string DeliveryAddress { get; set; }

        /// <summary>
        ///     Адрес отправления
        /// </summary>
        public string DepartureAddress { get; set; }
    }

    public class CreateOrderRequestValidator : AbstractValidator<CreateOrderRequest>
    {
        public CreateOrderRequestValidator()
        {
            RuleFor(x => x.AuthorId)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"AuthorId should be specified and non-empty GUID\UUID");

            RuleFor(x => x.Items)
                .NotEmpty()
                .WithMessage(@"There must be at least one order item");

            RuleFor(x => x.DeliveryAddress)
                .Must(x => !string.IsNullOrWhiteSpace(x) && x.Length <= Constants.AddressMaxLength)
                .WithMessage("Order must have a non-empty address no more than {Infrastructure.Constants.AddressMaxLength} characters");

            RuleFor(x => x.DepartureAddress)
                .Must(x => !string.IsNullOrWhiteSpace(x) && x.Length <= Constants.AddressMaxLength)
                .WithMessage("Order must have a non-empty address no more than {Infrastructure.Constants.AddressMaxLength} characters");

            RuleForEach(x => x.Items)
                .SetValidator(new CreateOrderItemRequestValidator<CreateOrderItemRequest>());
        }
    }
}