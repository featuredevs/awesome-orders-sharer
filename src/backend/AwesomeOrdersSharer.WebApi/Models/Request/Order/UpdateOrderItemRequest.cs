﻿namespace AwesomeOrdersSharer.WebApi.Models.Request.Order
{
    public class UpdateOrderItemRequest : CreateOrderItemRequest { }

    public class UpdateOrderItemRequestValidator : CreateOrderItemRequestValidator<UpdateOrderItemRequest> { }
}