﻿using System;
using AwesomeOrdersSharer.Domain.Common.Models;
using AwesomeOrdersSharer.WebApi.Infrastructure;
using FluentValidation;

namespace AwesomeOrdersSharer.WebApi.Models.Request.Order
{
    /// <summary>
    ///     Обновление заказа
    /// </summary>
    public class UpdateOrderRequest
    {
        /// <summary>
        ///     Статус заказа
        /// </summary>
        public OrderState? State { get; set; }

        /// <summary>
        ///     Исполнитель заказа
        /// </summary>
        public Guid? PerformerId { get; set; }

        /// <summary>
        ///     Адрес доставки
        /// </summary>
        public string DeliveryAddress { get; set; }

        /// <summary>
        ///     Адрес отправления
        /// </summary>
        public string DepartureAddress { get; set; }

        /// <summary>
        ///     Дата доставки
        /// </summary>
        public DateTime? DeliveryDate { get; set; }
    }

    public class UpdateOrderRequestValidator : AbstractValidator<UpdateOrderRequest>
    {
        public UpdateOrderRequestValidator()
        {
            When(x => x.State.HasValue, () =>
            {
                RuleFor(x => x.State.Value)
                    .IsInEnum()
                    .Must(x => x != OrderState.Undefined)
                    .WithMessage($"State should be not equal to {OrderState.Undefined} ({(int) OrderState.Undefined})");
            });

            When(x => x.PerformerId.HasValue, () =>
            {
                RuleFor(x => x.PerformerId.Value)
                    .Must(x => x != Guid.Empty && x != default)
                    .WithMessage(@"PerformerId should be specified and non-empty GUID\UUID");
            });

            When(x => x.DeliveryAddress != null, () =>
            {
                RuleFor(x => x.DeliveryAddress)
                    .Must(x => !string.IsNullOrWhiteSpace(x) && x.Length <= Constants.AddressMaxLength)
                    .WithMessage("Order must have a non-empty address no more than {Infrastructure.Constants.AddressMaxLength} characters");
            });

            When(x => x.DepartureAddress != null, () =>
            {
                RuleFor(x => x.DepartureAddress)
                    .Must(x => !string.IsNullOrWhiteSpace(x) && x.Length <= Constants.AddressMaxLength)
                    .WithMessage("Order must have a non-empty address no more than {Infrastructure.Constants.AddressMaxLength} characters");
            });
        }
    }
}