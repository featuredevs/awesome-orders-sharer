﻿namespace AwesomeOrdersSharer.WebApi.Models.Request.Order
{
    public class AddOrderItemRequest : CreateOrderItemRequest { }

    public class AddOrderItemRequestValidator : CreateOrderItemRequestValidator<AddOrderItemRequest> { }
}