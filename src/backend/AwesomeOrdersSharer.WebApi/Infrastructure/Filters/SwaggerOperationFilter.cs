﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace AwesomeOrdersSharer.WebApi.Infrastructure.Filters
{
    public class SwaggerOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            operation.Responses.Add("204", new OpenApiResponse {Description = "NoContent"});
            operation.Responses.Add("400", new OpenApiResponse {Description = "BadRequest"});
            operation.Responses.Add("404", new OpenApiResponse {Description = "NotFound"});
            operation.Responses.Add("500", new OpenApiResponse {Description = "InternalServerError"});

            // check for auth attribute and add responses if any
            if (context.MethodInfo.DeclaringType != null)
            {
                var anonymousAttributes = context.MethodInfo.DeclaringType.GetCustomAttributes(true)
                    .Union(context.MethodInfo.GetCustomAttributes(true))
                    .OfType<AllowAnonymousAttribute>();

                if (!anonymousAttributes.Any())
                {
                    operation.Responses.Add("401", new OpenApiResponse {Description = "Unauthorized"});
                    operation.Responses.Add("403", new OpenApiResponse {Description = "Forbidden"});
                }
            }
        }
    }
}