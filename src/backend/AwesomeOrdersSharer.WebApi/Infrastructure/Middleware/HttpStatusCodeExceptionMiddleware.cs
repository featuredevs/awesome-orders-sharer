﻿using System;
using System.Threading.Tasks;
using AwesomeOrdersSharer.Domain.Common.Extensions;
using AwesomeOrdersSharer.WebApi.Models.Response;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace AwesomeOrdersSharer.WebApi.Infrastructure.Middleware
{
    public class HttpStatusCodeExceptionMiddleware
    {
        private readonly ILogger<HttpStatusCodeExceptionMiddleware> _logger;
        private readonly RequestDelegate _next;

        public HttpStatusCodeExceptionMiddleware(RequestDelegate next, ILogger<HttpStatusCodeExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context,
            IWebHostEnvironment hostingEnvironment)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                if (context.Response.HasStarted)
                {
                    _logger.LogWarning(ex, "The response has already started, the http status code middleware will not be executed.");
                    throw;
                }

                context.Response.Clear();
                context.Response.ContentType = Constants.DefaultMimeType;

                if (!Constants.ExceptionMap.TryGetValue(ex.GetType(), out var statusCode))
                {
                    statusCode = StatusCodes.Status500InternalServerError;
                }

                var response = new ExceptionResponse(ex.Message, hostingEnvironment.IsDevelopment() ? ex.StackTrace : null);
                context.Response.StatusCode = statusCode;
                await context.Response.WriteAsync(response.SerializeToJson(), context.RequestAborted);
            }
        }
    }
}