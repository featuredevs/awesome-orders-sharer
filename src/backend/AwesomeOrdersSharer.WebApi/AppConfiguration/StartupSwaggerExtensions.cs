﻿using System;
using System.IO;
using System.Reflection;
using AwesomeOrdersSharer.WebApi.Infrastructure.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace AwesomeOrdersSharer.WebApi.AppConfiguration
{
    public static class StartupSwaggerExtensions
    {
        private static string EntryAssemblyName => Assembly.GetEntryAssembly()?.GetName().Name ?? "Unknown";

        public static IServiceCollection AddSwagger(this IServiceCollection services, IWebHostEnvironment hostEnvironment, string docName)
        {
            // swagger should be enabled only on dev environment

            if (!hostEnvironment.IsDevelopment())
            {
                return services;
            }

            services.AddSwaggerGen(c =>
            {
                var contact = new OpenApiContact
                {
                    Name = "FeatureDevs, OTUS",
                    Url = new Uri("https://gitlab.com/featuredevs")
                };
                c.SwaggerDoc(
                    EntryAssemblyName,
                    new OpenApiInfo
                    {
                        Title = $"AwesomeOrders {docName}",
                        Description = $"AwesomeOrders {docName} developer documentation",
                        Contact = contact
                    });

                c.DescribeAllParametersInCamelCase();

                c.EnableAnnotations(true, true);
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{EntryAssemblyName}.xml"));

                c.OperationFilter<SwaggerOperationFilter>();
            });

            return services;
        }


        public static IApplicationBuilder UseSwaggerAndConfigure(this IApplicationBuilder applicationBuilder,
            IWebHostEnvironment hostEnvironment, string docName)
        {
            // swagger should be enabled only on dev environment

            if (!hostEnvironment.IsDevelopment())
            {
                return applicationBuilder;
            }

            applicationBuilder.UseSwagger();
            applicationBuilder.UseSwaggerUI(c =>
            {
                c.DocumentTitle = $"AwesomeOrders {docName} documentation";
                c.SwaggerEndpoint($"/swagger/{EntryAssemblyName}/swagger.json", $"AwesomeOrders {docName}");
                c.DisplayRequestDuration();
                c.SupportedSubmitMethods(SubmitMethod.Get, SubmitMethod.Post, SubmitMethod.Put, SubmitMethod.Delete);
                c.DocExpansion(DocExpansion.List);
                c.DefaultModelRendering(ModelRendering.Model);
                c.DefaultModelExpandDepth(10);
                c.DefaultModelsExpandDepth(10);
                c.ShowExtensions();
            });

            return applicationBuilder;
        }
    }
}