﻿using System.Threading;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.Domain.Contracts.ServicesAbstractions
{
    public interface IMessageBrokerService
    {
        Task PublishAsync<TMessage>(TMessage message, CancellationToken cancellationToken = default) where TMessage : class;
    }
}
