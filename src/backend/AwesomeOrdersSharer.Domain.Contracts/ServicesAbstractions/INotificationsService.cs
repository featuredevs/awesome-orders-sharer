﻿using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.Domain.Contracts.ServicesAbstractions
{
    public interface INotificationsService
    {
        Task NotifyOrderCreated(OrderReadModel order);

        Task NotifyStatusUpdate(OrderReadModel order);
    }
}
