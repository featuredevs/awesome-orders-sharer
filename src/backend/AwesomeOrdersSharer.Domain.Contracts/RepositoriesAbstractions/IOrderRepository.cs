﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AwesomeOrdersSharer.Domain.Contracts.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;

namespace AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions
{
    public interface IOrderRepository
    {
        Task<OrderReadModel> GetOrderAsync(Guid id, CancellationToken cancellationToken);

        Task<PaginationReadModel<OrderReadModel>> SearchOrdersAsync(SearchOrdersFilter filter, CancellationToken cancellationToken);

        Task<OrderReadModel> CreateOrderAsync(CreateOrderModel order, CancellationToken cancellationToken);

        Task<OrderReadModel> UpdateOrderAsync(Guid id, UpdateOrderModel order, CancellationToken cancellationToken);

        Task DeleteOrderAsync(Guid id, CancellationToken cancellationToken);

        Task<OrderItemReadModel> AddOrderItemAsync(Guid orderId, CreateOrderItemModel model, CancellationToken cancellationToken);

        Task<OrderItemReadModel> UpdateOrderItemAsync(Guid orderId, Guid itemId, UpdateOrderItemModel model,
            CancellationToken cancellationToken);

        Task DeleteOrderItemAsync(Guid orderId, Guid itemId, CancellationToken cancellationToken);
    }
}