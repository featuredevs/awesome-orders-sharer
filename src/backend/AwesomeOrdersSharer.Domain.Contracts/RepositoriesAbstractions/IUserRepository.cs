﻿using AwesomeOrdersSharer.Domain.Contracts.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.User;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions
{
    public interface IUserRepository
    {
        Task<UserReadModel> GetUserAsync(Guid userId, CancellationToken cancellationToken);

        Task<PaginationReadModel<UserReadModel>> SearchUsersAsync(SearchUsersFilter filter, CancellationToken cancellationToken);

        Task UpsertUserAsync(UpsertUserModel user, CancellationToken cancellationToken);
    }
}