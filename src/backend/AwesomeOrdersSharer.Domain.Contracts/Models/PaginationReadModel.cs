﻿using System.Collections.Generic;

namespace AwesomeOrdersSharer.Domain.Contracts.Models
{
    public class PaginationReadModel<T> where T : class
    {
        // ReSharper disable once UnusedMember.Global - for testing purposes
        protected PaginationReadModel() { }

        public PaginationReadModel(IEnumerable<T> items, int count)
        {
            Items = items;
            Count = count;
        }

        public IEnumerable<T> Items { get; protected set; } = new List<T>();

        public int Count { get; protected set; }
    }
}