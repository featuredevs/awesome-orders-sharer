﻿using System;

namespace AwesomeOrdersSharer.Domain.Contracts.Models.MessageBroker
{
    public class EmailMessageModel
    {
        public EmailMessageModel(Guid userId, string subject, string body)
        {
            UserId = userId;
            Body = body;
            Subject = subject;
        }

        public Guid UserId { get; }

        public string Subject { get; }

        public string Body { get; }
    }
}
