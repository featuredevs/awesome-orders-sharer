﻿using System;
using System.Collections.Generic;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.Domain.Contracts.Models.Order
{
    public class OrderReadModel
    {
        protected OrderReadModel() { }

        public OrderReadModel(Guid id, OrderState state, string history, Guid authorId, Guid? performerId, long number,
            IEnumerable<OrderItemReadModel> items, string deliveryAddress, string departureAddress, DateTime? dateDelivered)
        {
            Id = id;
            State = state;
            History = history;
            AuthorId = authorId;
            PerformerId = performerId;
            Number = number;
            Items = items;
            DeliveryAddress = deliveryAddress;
            DepartureAddress = departureAddress;
            DateDelivered = dateDelivered;
        }

        public Guid Id { get; protected set; }

        public OrderState State { get; protected set; }

        public string History { get; protected set; }

        public Guid AuthorId { get; protected set; }

        public Guid? PerformerId { get; protected set; }

        public long Number { get; protected set; }

        public string DeliveryAddress { get; protected set; }

        public string DepartureAddress { get; protected set; }

        public DateTime? DateDelivered { get; protected set; }

        public IEnumerable<OrderItemReadModel> Items { get; protected set; }
    }
}