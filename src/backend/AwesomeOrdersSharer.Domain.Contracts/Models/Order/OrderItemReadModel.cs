﻿using System;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.Domain.Contracts.Models.Order
{
    public class OrderItemReadModel
    {
        protected OrderItemReadModel() { }

        public OrderItemReadModel(Guid id, Guid orderId, string description)
        {
            Id = id;
            OrderId = orderId;
            Description = description;
        }

        public Guid Id { get; protected set; }

        public Guid OrderId { get; protected set; }

        public string Description { get; protected set; }

        public OrderItemState State { get; protected set; }
    }
}