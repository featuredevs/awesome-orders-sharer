﻿namespace AwesomeOrdersSharer.Domain.Contracts.Models.Order
{
    public class UpdateOrderItemModel
    {
        protected UpdateOrderItemModel() { }

        public UpdateOrderItemModel(string description)
        {
            Description = description;
        }

        public string Description { get; protected set; }
    }
}