﻿using System;
using System.Collections.Generic;
using AwesomeOrdersSharer.DataAccess.Contracts.Models;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.Domain.Contracts.Models.Order
{
    public class CreateOrderModel
    {
        protected CreateOrderModel() { }

        public CreateOrderModel(Guid authorId, IEnumerable<CreateOrderItemModel> items, string deliveryAddress, string departureAddress)
        {
            AuthorId = authorId;
            Items = items;
            DeliveryAddress = deliveryAddress;
            DepartureAddress = departureAddress;
        }

        public Guid AuthorId { get; protected set; }

        public string DeliveryAddress { get; protected set; }

        public string DepartureAddress { get; protected set; }

        public IEnumerable<CreateOrderItemModel> Items { get; protected set; }

        public DbOrder ToDbOrder()
        {
            return new DbOrder((int) OrderState.Created, AuthorId, null, DeliveryAddress, DepartureAddress, null);
        }
    }
}