﻿using System;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.Domain.Contracts.Models.Order
{
    public class SearchOrdersFilter
    {
        protected SearchOrdersFilter() { }

        public SearchOrdersFilter(Guid? authorId, OrderState? state, Guid? performerId, int offset, int limit)
        {
            AuthorId = authorId;
            State = state;
            PerformerId = performerId;
            Offset = offset;
            Limit = limit;
        }

        public Guid? AuthorId { get; protected set; }

        public OrderState? State { get; protected set; }

        public Guid? PerformerId { get; protected set; }

        public int Offset { get; protected set; }

        public int Limit { get; protected set; }
    }
}