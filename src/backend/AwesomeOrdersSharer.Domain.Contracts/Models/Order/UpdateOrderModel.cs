﻿using System;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.Domain.Contracts.Models.Order
{
    public class UpdateOrderModel
    {
        protected UpdateOrderModel() { }

        public UpdateOrderModel(OrderState? state, Guid? performerId, DateTime? deliveryDate, string deliveryAddress,
            string departureAddress)
        {
            State = state;
            PerformerId = performerId;
            DeliveryDate = deliveryDate;
            DeliveryAddress = deliveryAddress;
            DepartureAddress = departureAddress;
        }

        public DateTime? DeliveryDate { get; protected set; }

        public string DeliveryAddress { get; protected set; }

        public string DepartureAddress { get; protected set; }

        public OrderState? State { get; protected set; }

        public Guid? PerformerId { get; protected set; }
    }
}