﻿using System;
using AwesomeOrdersSharer.Domain.Common.Extensions;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.Domain.Contracts.Models.User
{
    public class UserReadModel
    {
        protected UserReadModel() { }

        public UserReadModel(Guid id, string firstName, string lastName, string email, int role, int? dateOfBirth)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            DateOfBirth = dateOfBirth.ToBirthdayDate();
            Role = (UserRole) role;
        }

        public Guid Id { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public string Email { get; protected set; }
        public DateTime? DateOfBirth { get; protected set; }
        public UserRole Role { get; protected set; }
    }
}