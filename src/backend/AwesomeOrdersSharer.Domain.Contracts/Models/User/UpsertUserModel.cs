﻿using System;

// ReSharper disable once CheckNamespace for MassTransit Deserialization
namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models
{
    public class UpsertUserModel
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTimeOffset RegistrationDate { get; set; }

        public bool IsLocked { get; set; }

        public int? DateOfBirth { get; set; }

        public bool IsEmailConfirmed { get; set; }
    }
}