﻿using System;
using AwesomeOrdersSharer.Domain.Common.Models;

namespace AwesomeOrdersSharer.Domain.Contracts.Models.User
{
    public class SearchUsersFilter
    {
        protected SearchUsersFilter() { }

        public SearchUsersFilter(string textFitler, DateTime? dateOfBirth, UserRole? role, int offset,
            int limit)
        {
            TextFilter = textFitler;
            DateOfBirth = dateOfBirth;
            Role = role;
            Offset = offset;
            Limit = limit;
        }

        public string TextFilter { get; protected set; }
        public DateTime? DateOfBirth { get; protected set; }
        public UserRole? Role { get; protected set; }
        public int Offset { get; protected set; }
        public int Limit { get; protected set; }
    }
}