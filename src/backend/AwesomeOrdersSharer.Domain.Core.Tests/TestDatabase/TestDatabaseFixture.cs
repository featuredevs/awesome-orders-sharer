﻿using System;
using AwesomeOrdersSharer.DataAccess.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace AwesomeOrdersSharer.Domain.Core.Tests.TestDatabase
{
    public class TestDatabaseFixture : IDisposable
    {
        private readonly TestDatabaseInitializer _efTestDbInitializer;

        public TestDatabaseFixture()
        {
            var options = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase("TestingDb")
                .UseLazyLoadingProxies()
                .Options;
            DbContext = new TestDatabaseContext(options);
            _efTestDbInitializer = new TestDatabaseInitializer(DbContext);
            _efTestDbInitializer.InitializeDb();
        }

        public TestDatabaseContext DbContext { get; }

        public void Dispose()
        {
            _efTestDbInitializer.CleanDb();
        }
    }
}