﻿using Xunit;

namespace AwesomeOrdersSharer.Domain.Core.Tests.TestDatabase
{
    [CollectionDefinition(DbCollection)]
    public class TestDatabaseCollection : ICollectionFixture<TestDatabaseFixture>
    {
        public const string DbCollection = "Database collection";
    }
}