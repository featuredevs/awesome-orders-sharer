﻿using AwesomeOrdersSharer.DataAccess.EntityFramework;

namespace AwesomeOrdersSharer.Domain.Core.Tests.TestDatabase
{
    public class TestDatabaseInitializer
    {
        private readonly DatabaseContext _databaseContext;

        public TestDatabaseInitializer(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void InitializeDb()
        {
            _databaseContext.Database.EnsureCreated();
        }

        public void CleanDb()
        {
            _databaseContext.Database.EnsureDeleted();
        }
    }
}