﻿using System.Collections.Generic;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;
using AwesomeOrdersSharer.DataAccess.EntityFramework;
using AwesomeOrdersSharer.DataAccess.EntityFramework.PreProcessors;
using Microsoft.EntityFrameworkCore;

namespace AwesomeOrdersSharer.Domain.Core.Tests.TestDatabase
{
    public class TestDatabaseContext : DatabaseContext
    {
        public TestDatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(
                options,
                new List<IDbEntityPreProcessor>
                {
                    new ModificationTimePreProcessor(),
                    new CreationTimePreProcessor()
                }) { }
    }
}