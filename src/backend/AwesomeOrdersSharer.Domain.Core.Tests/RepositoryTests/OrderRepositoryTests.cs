﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AwesomeOrdersSharer.DataAccess.EntityFramework;
using AwesomeOrdersSharer.Domain.Common.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions;
using AwesomeOrdersSharer.Domain.Core.Exceptions;
using AwesomeOrdersSharer.Domain.Core.Repositories;
using AwesomeOrdersSharer.Domain.Core.Tests.Builders;
using AwesomeOrdersSharer.Domain.Core.Tests.TestDatabase;
using FluentAssertions;
using Xunit;

namespace AwesomeOrdersSharer.Domain.Core.Tests.RepositoryTests
{
    [Collection(TestDatabaseCollection.DbCollection)]
    public class OrderRepositoryTests : IClassFixture<TestDatabaseFixture>
    {
        private readonly IMapper _mapper;
        private readonly IOrderRepository _orderRepository;

        public OrderRepositoryTests(TestDatabaseFixture orderFixture)
        {
            _mapper = MapperBuilder.CreateMapper();
            _orderRepository = new OrderRepository(new UnitOfWork(orderFixture.DbContext), _mapper);
        }

        [Fact]
        public async Task CreateOrder_CorrectOrder_NumberShouldContainDateTimeNow()
        {
            var createOrderModel = _mapper.Map<CreateOrderModel>(OrderBuilder.CreateOrderRequest());

            var order = await _orderRepository.CreateOrderAsync(createOrderModel, CancellationToken.None);

            Assert.NotNull(order);
            order.Number.ToString().Should().Contain($"{DateTime.UtcNow:yyyyMMdd}");
        }

        [Fact]
        public async Task UpdateOrder_UpdateStatusOfCreatedOrder_CreatedStatusIsInHistory()
        {
            var updateOrderModel = _mapper.Map<UpdateOrderModel>(OrderBuilder.UpdateOrderStateRequest());

            var order = await _orderRepository.UpdateOrderAsync(OrderBuilder.Order2Id, updateOrderModel,
                CancellationToken.None);

            Assert.NotNull(order);
            order.History.Should().Contain($"\"state\":{(int) OrderState.Created},");
        }

        [Fact]
        public async Task AddOrderItem_NonExistingOrder_EntityNotFoundExceptionThrown()
        {
            var addOrderItemModel = _mapper.Map<CreateOrderItemModel>(OrderBuilder.CreateOrderItemRequest());
            var nonExistingOrderId = Guid.NewGuid();
            await Assert.ThrowsAsync<EntityNotFoundException>(() =>
                _orderRepository.AddOrderItemAsync(nonExistingOrderId, addOrderItemModel, CancellationToken.None));
        }

        [Fact]
        public async Task UpdateOrderItem_ExistingItemOfAnExistingOrder_NewItemDescriptionIsInOrder()
        {
            var order = await _orderRepository.GetOrderAsync(OrderBuilder.OrderId, CancellationToken.None);
            Assert.NotNull(order);
            order.Items.Should().NotContain(i => string.Equals(i.Description, OrderBuilder.OrderItemDescription));

            var updateOrderItemModel = _mapper.Map<UpdateOrderItemModel>(OrderBuilder.UpdateOrderItemRequest());
            await _orderRepository.UpdateOrderItemAsync(order.Id, OrderBuilder.OrderItemId, updateOrderItemModel, CancellationToken.None);

            order = await _orderRepository.GetOrderAsync(OrderBuilder.OrderId, CancellationToken.None);
            order.Items.Should().Contain(i => string.Equals(i.Description, OrderBuilder.OrderItemDescription));
        }

        [Fact]
        public async Task DeleteOrderItem_OrderHasTwoItems_OneItemLeft()
        {
            var order = await _orderRepository.GetOrderAsync(OrderBuilder.OrderId, CancellationToken.None);
            Assert.NotNull(order);
            Assert.NotNull(order.Items);
            order.Items.Count().Should().Be(2);

            await _orderRepository.DeleteOrderItemAsync(OrderBuilder.OrderId, OrderBuilder.OrderItemId, CancellationToken.None);
            order = await _orderRepository.GetOrderAsync(OrderBuilder.OrderId, CancellationToken.None);

            order.Items.Count(i => i.State != OrderItemState.Deleted).Should().Be(1);
        }
    }
}