﻿using System.Collections.Generic;
using System.Threading;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using AwesomeOrdersSharer.Domain.Common.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions;
using AwesomeOrdersSharer.Domain.Core.Tests.Builders;
using AwesomeOrdersSharer.WebApi.Controllers;
using AwesomeOrdersSharer.WebApi.Models.Request;
using AwesomeOrdersSharer.WebApi.Models.Request.Order;
using AwesomeOrdersSharer.WebApi.Models.Response;
using AwesomeOrdersSharer.WebApi.Models.Response.Order;
using FluentAssertions;
using FluentValidation.TestHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace AwesomeOrdersSharer.Domain.Core.Tests.ControllerTests
{
    public class OrderControllerTests
    {
        private readonly IMapper _mapper;
        private readonly OrderController _orderController;

        private readonly Mock<IOrderRepository> _orderRepoMock;

        public OrderControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _orderRepoMock = fixture.Freeze<Mock<IOrderRepository>>();
            _mapper = MapperBuilder.CreateMapper();
            fixture.Inject(_mapper);
            _orderController = fixture.Build<OrderController>().OmitAutoProperties().Create();
            _orderController.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    RequestAborted = CancellationToken.None
                }
            };
        }

        [Fact]
        public void CreateOrder_OrderWithNoItems_ReturnsOneValidationError()
        {
            var createOrderRequest = OrderBuilder.CreateOrderRequest();
            createOrderRequest.Items = null;
            var validator = new CreateOrderRequestValidator();

            var validatorTestResult = validator.TestValidate(createOrderRequest);

            validatorTestResult.ShouldHaveValidationErrorFor(cor => cor.Items);
            validatorTestResult.ShouldNotHaveValidationErrorFor(cor => cor.AuthorId);
            validatorTestResult.ShouldNotHaveValidationErrorFor(cor => cor.DeliveryAddress);
            validatorTestResult.ShouldNotHaveValidationErrorFor(cor => cor.DepartureAddress);
        }

        [Fact]
        public async void DeleteOrder_ExistingOrder_DeletedOneOrder()
        {
            var request = new ByGuidRequest {Id = OrderBuilder.OrderId};

            await _orderController.DeleteOrder(request);

            _orderRepoMock.Verify(r => r.DeleteOrderAsync(request.Id, CancellationToken.None), Times.Once);
        }

        [Fact]
        public async void GetOrder_BaseOrder_OrderReturnedOk()
        {
            var orderReadModel = OrderBuilder.OrderReadModel();
            _orderRepoMock.Setup(r => r.GetOrderAsync(orderReadModel.Id, CancellationToken.None)).ReturnsAsync(orderReadModel);
            var request = new ByGuidRequest {Id = orderReadModel.Id};

            var result = await _orderController.GetOrder(request);

            result.Should().BeAssignableTo<OkObjectResult>();
            var model = ((OkObjectResult) result).Value as OrderResponse;
            model.Should().BeEquivalentTo(_mapper.Map<OrderResponse>(orderReadModel));
        }

        [Fact]
        public async void SearchOrders_DeletedState_ItemsCountEqualsZero()
        {
            var searchOrdersRequest = new SearchOrdersRequest
            {
                Limit = 100,
                Offset = 0,
                State = OrderState.Deleted
            };
            var expected = new PaginationReadModel<OrderReadModel>(new List<OrderReadModel>(), 0);
            _orderRepoMock.Setup(r => r.SearchOrdersAsync(_mapper.Map<SearchOrdersFilter>(searchOrdersRequest), CancellationToken.None))
                .ReturnsAsync(expected);

            var result = await _orderController.SearchOrders(searchOrdersRequest);

            result.Should().BeAssignableTo<OkObjectResult>();
            var model = ((OkObjectResult) result).Value as PaginationResponse<OrderResponse>;
            Assert.NotNull(model);
            model.Count.Should().Be(0);
        }
    }
}