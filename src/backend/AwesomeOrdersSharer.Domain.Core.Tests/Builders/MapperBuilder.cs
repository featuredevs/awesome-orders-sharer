using AutoMapper;
using AwesomeOrdersSharer.WebApi.Models.MappingProfiles;

namespace AwesomeOrdersSharer.Domain.Core.Tests.Builders
{
    public static class MapperBuilder
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.DisableConstructorMapping();
                    cfg.AddProfile<RequestMappingProfile>();
                    cfg.AddProfile<ResponseMappingProfile>();
                    cfg.AddProfile<DbMappingProfile>();
                });
            return config.CreateMapper();
        }
    }
}