﻿using System;
using System.Collections.Generic;
using AwesomeOrdersSharer.Domain.Common.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.WebApi.Models.Request.Order;

namespace AwesomeOrdersSharer.Domain.Core.Tests.Builders
{
    public static class OrderBuilder
    {
        public const string OrderItemDescription = "Лекарства";
        private const string DeliveryAddress = "Москва, ул. Братеевская, д.12, кв.14";
        private const string DepartureAddress = "Москва, ул. Пушкина, д.112, кв.141";
        private const string DefaultHistory = "[]";
        private const int Number = 12;
        public static readonly Guid OrderId = Guid.Parse("27c32201-9482-413d-bbcb-e48851f5f72e");
        public static readonly Guid Order2Id = Guid.Parse("bfaf123c-dfaf-4ba6-92d4-57c957d995dc");
        public static readonly Guid OrderItemId = Guid.Parse("375e37db-f7e6-4dbb-8480-93d69805c212");
        private static readonly Guid AuthorId = Guid.Parse("98a70d98-f9d6-46b5-a8d9-785ca930f839");
        private static readonly Guid PerformerId = Guid.Parse("5a4e73a9-e946-4d65-8262-6573c896e74f");
        private static readonly DateTime DeliveryDate = new DateTime(2020, 01, 05);

        public static CreateOrderRequest CreateOrderRequest()
        {
            return new CreateOrderRequest
            {
                DeliveryAddress = DeliveryAddress,
                DepartureAddress = DepartureAddress,
                AuthorId = AuthorId,
                Items = new List<CreateOrderItemRequest>
                {
                    new CreateOrderItemRequest
                    {
                        Description = OrderItemDescription
                    }
                }
            };
        }

        public static UpdateOrderRequest UpdateOrderStateRequest()
        {
            return new UpdateOrderRequest
            {
                State = OrderState.Assigned
            };
        }

        public static OrderReadModel OrderReadModel()
        {
            return new OrderReadModel(
                OrderId,
                OrderState.Created,
                DefaultHistory,
                AuthorId,
                PerformerId,
                Number,
                new List<OrderItemReadModel>
                {
                    new OrderItemReadModel(OrderItemId, OrderId, OrderItemDescription)
                },
                DeliveryAddress,
                DepartureAddress,
                DeliveryDate);
        }

        public static CreateOrderItemRequest CreateOrderItemRequest()
        {
            return new CreateOrderItemRequest
            {
                Description = OrderItemDescription
            };
        }

        public static UpdateOrderItemRequest UpdateOrderItemRequest()
        {
            return new UpdateOrderItemRequest
            {
                Description = OrderItemDescription
            };
        }
    }
}