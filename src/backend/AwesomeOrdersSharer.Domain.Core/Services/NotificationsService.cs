﻿using AwesomeOrdersSharer.Domain.Contracts.Models.MessageBroker;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.Domain.Contracts.ServicesAbstractions;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.Domain.Core.Services
{
    public class NotificationsService : INotificationsService
    {
        private readonly IMessageBrokerService _messageBrokerService;

        public NotificationsService(IMessageBrokerService messageBrokerService)
        {
            _messageBrokerService = messageBrokerService;
        }

        public async Task NotifyOrderCreated(OrderReadModel order)
        {
            var subject = $@"Заказ #{order.Number} создан";
            var body = @"Добрый день!.<br/>" +
                       $"Заказ #{order.Number} создан. <br/>";
            var emailMessage = new EmailMessageModel(order.AuthorId, subject, body);
            await _messageBrokerService.PublishAsync(emailMessage);
        }

        public async Task NotifyStatusUpdate(OrderReadModel order)
        {
            var subject = $@"Новый статус заказа #{order.Number}";
            var body = @"Добрый день!.<br/>" +
                       $"У заказа #{order.Number} изменился статус. <br/>" +
                       $"Новый статус: {order.State}";
            var emailMessage = new EmailMessageModel(order.AuthorId, subject, body);
            await _messageBrokerService.PublishAsync(emailMessage);
        }
    }
}
