﻿using AwesomeOrdersSharer.Domain.Contracts.ServicesAbstractions;
using MassTransit;
using System.Threading;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.Domain.Core.Services
{
    public class MessageBrokerService : IMessageBrokerService
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public MessageBrokerService(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task PublishAsync<TMessage>(TMessage message, CancellationToken cancellationToken = default) where TMessage : class
        {
            await _publishEndpoint.Publish(message, cancellationToken);
        }
    }
}
