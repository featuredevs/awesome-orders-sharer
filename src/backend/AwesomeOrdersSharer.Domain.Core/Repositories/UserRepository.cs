﻿using AutoMapper;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;
using AwesomeOrdersSharer.DataAccess.Contracts.Models;
using AwesomeOrdersSharer.Domain.Common.Extensions;
using AwesomeOrdersSharer.Domain.Contracts.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.User;
using AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions;
using AwesomeOrdersSharer.Domain.Core.Exceptions;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.Domain.Core.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UserRepository(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<UserReadModel> GetUserAsync(Guid userId, CancellationToken cancellationToken)
        {
            var user = await GetDbUserAsync(userId, cancellationToken);
            return _mapper.Map<UserReadModel>(user);
        }

        public async Task<PaginationReadModel<UserReadModel>> SearchUsersAsync(SearchUsersFilter filter,
            CancellationToken cancellationToken)
        {
            var query = _unitOfWork.Get<DbUser>();

            if (!string.IsNullOrWhiteSpace(filter.TextFilter))
            {
                query = query.Where(x => EF.Functions.Like(x.Email, $"{filter.TextFilter}%")
                                         || EF.Functions.Like(x.FirstName, $"{filter.TextFilter}%")
                                         || EF.Functions.Like(x.LastName, $"{filter.TextFilter}%"));
            }

            if (filter.DateOfBirth.HasValue)
            {
                var filterDateOfBirth = filter.DateOfBirth.ToBirthdayInt();
                query = query.Where(x => x.DateOfBirth == filterDateOfBirth);
            }

            if (filter.Role.HasValue)
            {
                var filterRole = (int)filter.Role.Value;
                query = query.Where(x => x.Role == filterRole);
            }

            var count = await query.CountAsync(cancellationToken);
            var users = await query
                .Skip(filter.Offset)
                .Take(filter.Limit)
                .Select(x => _mapper.Map<UserReadModel>(x))
                .ToListAsync(cancellationToken);

            return new PaginationReadModel<UserReadModel>(users, count);
        }

        public async Task UpsertUserAsync(UpsertUserModel user, CancellationToken cancellationToken)
        {
            var userFromDb = await _unitOfWork.Get<DbUser>().FirstOrDefaultAsync(u => u.Id == user.Id, cancellationToken);
            if (userFromDb == null)
            {
                _unitOfWork.Create(_mapper.Map<DbUser>(user));
                await _unitOfWork.CommitAsync(cancellationToken);
            }
            else
            {
                userFromDb.Update(user.FirstName, user.LastName, user.Email, user.DateOfBirth.ToBirthdayDate());
            }
        }

        private async Task<DbUser> GetDbUserAsync(Guid userId, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.Get<DbUser>().FirstOrDefaultAsync(o => o.Id == userId, cancellationToken);
            if (user == null)
            {
                throw EntityNotFoundException.CreateFromEntity<DbUser, Guid>(userId);
            }

            return user;
        }
    }
}