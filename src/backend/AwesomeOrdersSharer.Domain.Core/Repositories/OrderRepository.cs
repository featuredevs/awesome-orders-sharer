﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AwesomeOrdersSharer.DataAccess.Contracts.Abstractions;
using AwesomeOrdersSharer.DataAccess.Contracts.Models;
using AwesomeOrdersSharer.Domain.Common;
using AwesomeOrdersSharer.Domain.Common.Extensions;
using AwesomeOrdersSharer.Domain.Common.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models;
using AwesomeOrdersSharer.Domain.Contracts.Models.Order;
using AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions;
using AwesomeOrdersSharer.Domain.Core.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace AwesomeOrdersSharer.Domain.Core.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public OrderRepository(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<OrderReadModel> GetOrderAsync(Guid id, CancellationToken cancellationToken)
        {
            var dbOrder = await GetEntityAsync<DbOrder>(id, cancellationToken);
            return _mapper.Map<OrderReadModel>(dbOrder);
        }

        public async Task<PaginationReadModel<OrderReadModel>> SearchOrdersAsync(SearchOrdersFilter filter,
            CancellationToken cancellationToken)
        {
            var query = _unitOfWork.Get<DbOrder>();
            if (filter.State.HasValue)
            {
                // state needs to be precomputed, otherwise it's npgsql error
                var filterState = (int) filter.State.Value;
                query = query.Where(o => o.State == filterState);
            }

            if (filter.AuthorId.HasValue)
            {
                query = query.Where(o => o.AuthorId == filter.AuthorId.Value);
            }

            if (filter.PerformerId.HasValue)
            {
                query = query.Where(o => o.PerformerId == filter.PerformerId.Value);
            }

            var count = await query.CountAsync(cancellationToken);
            var orders = await query
                .Skip(filter.Offset)
                .Take(filter.Limit)
                .Select(o => _mapper.Map<OrderReadModel>(o))
                .ToListAsync(cancellationToken);

            return new PaginationReadModel<OrderReadModel>(orders, count);
        }

        public async Task<OrderReadModel> CreateOrderAsync(CreateOrderModel order, CancellationToken cancellationToken)
        {
            var dbOrder = _mapper.Map<DbOrder>(order);
            dbOrder.Number = CalculateOrderNumber();
            dbOrder = _unitOfWork.Create(dbOrder);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<OrderReadModel>(dbOrder);
        }

        public async Task<OrderReadModel> UpdateOrderAsync(Guid id, UpdateOrderModel order, CancellationToken cancellationToken)
        {
            var dbOrder = await GetEntityAsync<DbOrder>(id, cancellationToken);

            dbOrder.OrderHistory = GenerateHistoryString(dbOrder);
            dbOrder.Update((int?) order.State, order.PerformerId, order.DeliveryAddress, order.DepartureAddress,
                order.DeliveryDate?.ToUniversalTime());
            await _unitOfWork.CommitAsync(cancellationToken);

            return _mapper.Map<OrderReadModel>(dbOrder);
        }

        public async Task DeleteOrderAsync(Guid id, CancellationToken cancellationToken)
        {
            var dbOrder = await GetEntityAsync<DbOrder>(id, cancellationToken);
            dbOrder.OrderHistory = GenerateHistoryString(dbOrder);
            dbOrder.State = (int) OrderState.Deleted;
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        public async Task<OrderItemReadModel> AddOrderItemAsync(Guid orderId, CreateOrderItemModel model,
            CancellationToken cancellationToken)
        {
            var dbOrder = await GetEntityAsync<DbOrder>(orderId, cancellationToken);
            dbOrder.OrderHistory = GenerateHistoryString(dbOrder);
            var dbOrderItem = _mapper.Map<DbOrderItem>(model);
            dbOrderItem.OrderId = orderId;
            dbOrderItem = _unitOfWork.Create(dbOrderItem);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<OrderItemReadModel>(dbOrderItem);
        }

        public async Task<OrderItemReadModel> UpdateOrderItemAsync(Guid orderId, Guid itemId, UpdateOrderItemModel model,
            CancellationToken cancellationToken)
        {
            var dbOrder = await GetEntityAsync<DbOrder>(orderId, cancellationToken);
            var dbOrderItem = await GetEntityAsync<DbOrderItem>(itemId, cancellationToken);
            dbOrder.OrderHistory = GenerateHistoryString(dbOrder);
            dbOrderItem.Update(model.Description);
            await _unitOfWork.CommitAsync(cancellationToken);

            return _mapper.Map<OrderItemReadModel>(dbOrderItem);
        }

        public async Task DeleteOrderItemAsync(Guid orderId, Guid itemId, CancellationToken cancellationToken)
        {
            var dbOrder = await GetEntityAsync<DbOrder>(orderId, cancellationToken);
            var dbOrderItem = await GetEntityAsync<DbOrderItem>(itemId, cancellationToken);
            if (dbOrder.Items.Count == 1)
            {
                throw new InvalidOperationException("Can't delete the only order's item");
            }

            dbOrder.OrderHistory = GenerateHistoryString(dbOrder);
            dbOrderItem.State = (int) OrderItemState.Deleted;
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        private static long CalculateOrderNumber()
        {
            var dt = DateTimeOffset.UtcNow;
            var second = (double) (dt.Second > 0 ? dt.Second : Constants.RandomSecondValue);
            var numberPart = (long) Math.Round(dt.Hour * dt.Minute * dt.Millisecond / second);

            return long.Parse($"{dt:yyyyMMdd}{numberPart}");
        }

        private static string GenerateHistoryString(DbOrder order)
        {
            var history = order.OrderHistory.DeserializeFromJson<ICollection<OrderHistory>>();
            history.Add(new OrderHistory
            {
                State = order.State,
                PerformerId = order.PerformerId,
                DeliveryAddress = order.DeliveryAddress,
                DepartureAddress = order.DepartureAddress,
                DateDelivered = order.DateDelivered,
                Items = order.Items.Select(i => new OrderHistoryItem
                {
                    Id = i.Id,
                    Description = i.Description
                })
            });
            return history.SerializeToJson();
        }

        private async Task<T> GetEntityAsync<T>(Guid id, CancellationToken cancellationToken) where T : BaseGuidEntity
        {
            var dbEntity = await _unitOfWork.Get<T>().FirstOrDefaultAsync(o => o.Id == id, cancellationToken);
            if (dbEntity == null)
            {
                throw EntityNotFoundException.CreateFromEntity<T, Guid>(id);
            }

            return dbEntity;
        }

        private class OrderHistory
        {
            public int State { get; set; }

            public DateTimeOffset? DateDelivered { get; set; }

            public string DeliveryAddress { get; set; }
            public string DepartureAddress { get; set; }

            public Guid? PerformerId { get; set; }

            public IEnumerable<OrderHistoryItem> Items { get; set; } = new List<OrderHistoryItem>();
        }

        private class OrderHistoryItem
        {
            public Guid Id { get; set; }

            public string Description { get; set; }
        }
    }
}