﻿using Autofac;
using AwesomeOrdersSharer.Domain.Contracts.RepositoriesAbstractions;
using AwesomeOrdersSharer.Domain.Core.Repositories;

namespace AwesomeOrdersSharer.Domain.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static ContainerBuilder AddCore(this ContainerBuilder builder)
        {
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerLifetimeScope();
            builder.RegisterType<OrderRepository>().As<IOrderRepository>().InstancePerLifetimeScope();
            return builder;
        }
    }
}