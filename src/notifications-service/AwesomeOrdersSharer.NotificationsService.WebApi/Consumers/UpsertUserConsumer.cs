﻿using AutoMapper;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using AwesomeOrdersSharer.NotificationsService.Core.Abstractions;
using AwesomeOrdersSharer.NotificationsService.Core.Models;
using MassTransit;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.NotificationsService.WebApi.Consumers
{
    public class UpsertUserConsumer : IConsumer<UpsertUserModel>
    {
        private readonly IRepository<User> _userRepository;
        private readonly IMapper _mapper;

        public UpsertUserConsumer(IRepository<User> userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<UpsertUserModel> context)
        {
            await _userRepository.UpsertAsync(_mapper.Map<User>(context.Message));
        }
    }
}
