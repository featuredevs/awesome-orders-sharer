﻿using AwesomeOrdersSharer.Domain.Contracts.Models.MessageBroker;
using AwesomeOrdersSharer.NotificationsService.Core.Abstractions;
using AwesomeOrdersSharer.NotificationsService.Core.Exceptions;
using AwesomeOrdersSharer.NotificationsService.Core.Models;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.NotificationsService.WebApi.Consumers
{
    public class SendEmailConsumer : IConsumer<EmailMessageModel>
    {
        private readonly IEmailService _emailService;
        private readonly IRepository<User> _userRepository;

        public SendEmailConsumer(IEmailService emailService, IRepository<User> userRepository)
        {
            _emailService = emailService;
            _userRepository = userRepository;
        }

        public async Task Consume(ConsumeContext<EmailMessageModel> context)
        {
            var user = await _userRepository.GetByIdAsync(context.Message.UserId);
            if (user == null)
            {
                throw EntityNotFoundException.CreateFromEntity<User, Guid>(context.Message.UserId);
            }

            var emailMessage = new EmailMessage(context.Message.Subject, user.Email, context.Message.Body);
            await _emailService.Send(emailMessage);
        }
    }
}
