using AutoMapper;
using AwesomeOrdersSharer.NotificationsService.Core.Abstractions;
using AwesomeOrdersSharer.NotificationsService.Core.Services;
using AwesomeOrdersSharer.NotificationsService.DataAccess.MongoDb;
using AwesomeOrdersSharer.NotificationsService.DataAccess.Repositories;
using AwesomeOrdersSharer.NotificationsService.WebApi.Consumers;
using AwesomeOrdersSharer.NotificationsService.WebApi.Infrastructure.Middleware;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Reflection;

namespace AwesomeOrdersSharer.NotificationsService.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddOptions<EmailSettings>().Bind(Configuration.GetSection(nameof(EmailSettings)));
            services.AddTransient<IEmailService, EmailService>();

            services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));
            services.AddOptions<MongoDbSettings>().Bind(Configuration.GetSection(nameof(MongoDbSettings)));

            services.AddMassTransit(config =>
            {
                config.AddConsumer<SendEmailConsumer>();
                config.AddConsumer<UpsertUserConsumer>();
                config.UsingRabbitMq((context, factoryConfig) =>
                {
                    factoryConfig.Host(new Uri(Configuration["RabbitMq:Uri"]));
                    factoryConfig.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<HttpStatusCodeExceptionMiddleware>();

            app.UseSerilogRequestLogging();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
        }
    }
}
