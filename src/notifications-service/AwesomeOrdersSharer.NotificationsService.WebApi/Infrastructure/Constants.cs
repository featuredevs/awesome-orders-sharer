﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace AwesomeOrdersSharer.NotificationsService.WebApi.Infrastructure
{
    public class Constants
    {
        public const string DefaultMimeType = "application/json";

        public static readonly Dictionary<Type, int> ExceptionMap = new Dictionary<Type, int>
        {
            {typeof(EntryPointNotFoundException), StatusCodes.Status404NotFound}
        };
    }
}
