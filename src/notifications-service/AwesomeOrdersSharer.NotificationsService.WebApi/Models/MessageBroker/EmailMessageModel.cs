﻿using System;

// ReSharper disable once CheckNamespace for MassTransit
namespace AwesomeOrdersSharer.Domain.Contracts.Models.MessageBroker
{
    public class EmailMessageModel
    {
        public string Subject { get; set; }

        public Guid UserId { get; set; }

        public string Body { get; set; }
    }
}
