﻿using System;

// ReSharper disable once CheckNamespace for MassTransit Deserialization
namespace AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models
{
    public class UpsertUserModel
    {
        public Guid Id { get; set; }

        public string Email { get; set; }
    }
}
