﻿using AutoMapper;
using AwesomeOrdersSharer.IdentityService.Presentation.Infrastructure.Data.Models;
using AwesomeOrdersSharer.NotificationsService.Core.Models;

namespace AwesomeOrdersSharer.NotificationsService.WebApi.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UpsertUserModel, User>();
        }
    }
}