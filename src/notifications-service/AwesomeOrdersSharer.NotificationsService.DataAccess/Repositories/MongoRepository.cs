﻿using AwesomeOrdersSharer.NotificationsService.Core.Abstractions;
using AwesomeOrdersSharer.NotificationsService.Core.Models;
using AwesomeOrdersSharer.NotificationsService.DataAccess.MongoDb;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.NotificationsService.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _entities;

        public MongoRepository(IOptionsSnapshot<MongoDbSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            var database = client.GetDatabase(settings.Value.DatabaseName);
            _entities = database.GetCollection<T>($"{typeof(T).Name}s");
        }

        public async Task AddAsync(T entity)
        {
            await _entities.InsertOneAsync(entity);
        }

        public async Task UpsertAsync(T entity)
        {
            var existingEntity = await GetByIdAsync(entity.Id);
            if (existingEntity == null)
            {
                await AddAsync(entity);
                return;
            }

            await UpdateAsync(entity.Id, entity);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _entities.DeleteOneAsync(e => e.Id == id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _entities.Find(e => true).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _entities.Find(e => e.Id == id).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(Guid id, T entity)
        {
            entity.Id = id;
            await _entities.ReplaceOneAsync(e => e.Id == entity.Id, entity);
        }
    }
}
