﻿using System;

namespace AwesomeOrdersSharer.NotificationsService.Core.Models
{
    public class User : BaseEntity
    {
        protected User() { }

        public User(Guid id, string email)
        {
            Id = id;
            Email = email;
        }

        public string Email { get; protected set; }
    }
}
