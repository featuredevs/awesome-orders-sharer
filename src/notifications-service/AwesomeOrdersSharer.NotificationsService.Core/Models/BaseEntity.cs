﻿using System;

namespace AwesomeOrdersSharer.NotificationsService.Core.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
