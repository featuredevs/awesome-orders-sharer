﻿namespace AwesomeOrdersSharer.NotificationsService.Core.Models
{
    public class EmailMessage
    {
        protected EmailMessage() { }

        public EmailMessage(string subject, string email, string body)
        {
            Subject = subject;
            Email = email;
            Body = body;
        }

        public string Subject { get; protected set; }

        public string Email { get; protected set; }

        public string Body { get; protected set; }
    }
}
