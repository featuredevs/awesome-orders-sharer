﻿using AwesomeOrdersSharer.NotificationsService.Core.Models;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.NotificationsService.Core.Abstractions
{
    public interface IEmailService
    {
        Task Send(EmailMessage email);
    }
}
