﻿using AwesomeOrdersSharer.NotificationsService.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.NotificationsService.Core.Abstractions
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task AddAsync(T entity);

        Task UpdateAsync(Guid id, T entity);

        Task UpsertAsync(T entity);

        Task DeleteAsync(Guid id);
    }
}
