﻿namespace AwesomeOrdersSharer.NotificationsService.Core.Services
{
    public class EmailSettings
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public string Server { get; set; }

        public int Port { get; set; }

        public bool EnableSsl { get; set; }
    }
}
