﻿using AwesomeOrdersSharer.NotificationsService.Core.Abstractions;
using AwesomeOrdersSharer.NotificationsService.Core.Models;
using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AwesomeOrdersSharer.NotificationsService.Core.Services
{
    public class EmailService : IEmailService
    {
        private readonly IOptionsSnapshot<EmailSettings> _emailSettings;

        public EmailService(IOptionsSnapshot<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings;
        }

        public async Task Send(EmailMessage email)
        {
            var from = new MailAddress(_emailSettings.Value.Email, _emailSettings.Value.Name);
            var to = new MailAddress(email.Email);
            var message = new MailMessage(from, to)
            {
                Subject = email.Subject,
                Body = email.Body,
                IsBodyHtml = true
            };
            using var smtp = new SmtpClient(_emailSettings.Value.Server, _emailSettings.Value.Port)
            {
                Credentials = new NetworkCredential(_emailSettings.Value.Email, _emailSettings.Value.Password),
                EnableSsl = _emailSettings.Value.EnableSsl,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };

            await smtp.SendMailAsync(message);
        }
    }
}
