export const UserRole = {
    Unassigned: 0,
    User: 1,
    Courier: 2,
    Manager: 3
}