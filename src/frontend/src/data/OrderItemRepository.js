export const OrderItemRepository = [
    {id: '39dc635c-4a54-4b10-8d7e-accb30d1d800', description: 'Кока-кола'},
    {id: 'ac91ecb1-aeca-4613-a13c-706c43decdb2', description: 'Пепси-кола'},
    {id: '2db3a2a3-ecc0-49e3-8bcc-2ad5fc1ce0ae', description: 'Морс'},
    {id: '5d8afda5-c93f-4b1a-ae5d-d8c7be6ec7c5', description: 'Пица'},
    {id: '5002e4a1-c473-4268-957e-f60e3e6c0de7', description: 'Бургер'},
    {id: '120841cf-ea26-4442-b9e9-5397b42afe34', description: 'Шаурма'},
    {id: '6db73736-fce6-4e52-b76d-1d69605c9ce0', description: 'Бурито'},
    {id: '384e2fa9-7394-493e-8c61-60e85ed289bb', description: 'Донер'},
    {id: 'ea20789d-ab3e-4030-a998-4169598a7950', description: 'Хотдог'},
    {id: '83235369-c3e0-450e-b536-68ae403d8e54', description: 'Гамбургер'},
]