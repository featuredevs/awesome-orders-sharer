import {OrderItemRepository} from './OrderItemRepository'

export const OrderResponse = [
    {
        id: '5dbf4a3c-5402-4a94-8127-74a261d2ac44',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: 'e9224a29-bf02-4dd4-b182-a75c25afa0f6',
        items: [
            OrderItemRepository[0].id,
            OrderItemRepository[3].id,
            OrderItemRepository[4].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: '349ec0ef-6a32-415d-95c7-11bd5f69aa43',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: '7124e82b-eacf-43f6-99fb-1cccd66b8f14',
        items: [
            OrderItemRepository[1].id,
            OrderItemRepository[4].id,
            OrderItemRepository[5].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: 'd958ac92-5cea-4bb4-9f17-16fcf2bbf476',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: '5ee63591-a44e-4c5d-a662-b3430ffd3a46',
        items: [
            OrderItemRepository[2].id,
            OrderItemRepository[5].id,
            OrderItemRepository[6].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: '807e2707-32ea-408b-8850-5dfa3a824dbc',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: '29d6230c-66b9-447e-952d-c50d9fa75aa1',
        items: [
            OrderItemRepository[0].id,
            OrderItemRepository[6].id,
            OrderItemRepository[7].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: '7d1892b0-c54f-4379-a0c5-950c76e300e2',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: '9dfe4a89-d841-488c-a816-79ad6ae95f8c',
        items: [
            OrderItemRepository[1].id,
            OrderItemRepository[7].id,
            OrderItemRepository[8].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: '769e71c8-b515-4b1d-aa51-07f2ed0b24a8',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: '03c3ea50-8f8a-4e03-b412-948985db294f',
        items: [
            OrderItemRepository[2].id,
            OrderItemRepository[8].id,
            OrderItemRepository[9].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: 'fa00b714-d16b-4d7a-8c41-857c1389a57b',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: 'd5ca46c6-bb36-412f-a161-a13685632175',
        items: [
            OrderItemRepository[0].id,
            OrderItemRepository[3].id,
            OrderItemRepository[4].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: '4a07b335-df57-4ed2-bed8-4c1ba838990c',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: 'c4dabd32-272a-4118-9970-4441fd08dca3',
        items: [
            OrderItemRepository[1].id,
            OrderItemRepository[4].id,
            OrderItemRepository[5].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: 'caf18198-be84-43b2-a75f-9b17652780a1',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: '7c761597-6efb-4111-be30-b942aefa3240',
        items: [
            OrderItemRepository[2].id,
            OrderItemRepository[5].id,
            OrderItemRepository[6].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
    {
        id: '7dbdaac8-5bfd-4ca7-9633-11c48506f4fd',
        state: '1',
        authorId: '574f6575-f695-4dc1-b7d4-0997a84ed10f',
        performerId: 'dbb9432c-0e8a-44b8-b55b-4a9d7df6f433',
        number: '40fe7dcd-b447-454c-a3ca-238dd6f70c26',
        items: [
            OrderItemRepository[2].id,
            OrderItemRepository[5].id,
            OrderItemRepository[7].id,
        ],
        address: 'Silent street 911',
        deliveryDate: (new Date()).toDateString()
    },
]