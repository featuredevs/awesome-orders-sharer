export const Url = {
    auth: '/',
    home: '/',
    about: '/about',
    create: '/orders/create',
    history: '/orders/history',
    profile: {
        user: '/profile/user',
        courier: "/profile/courier",
        manager: "/profile/manager",
    }
}