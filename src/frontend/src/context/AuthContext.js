import {createContext} from 'react'
import {UserRole} from '../data/UserRole'

function noop() {}

export const AuthContext = createContext({
  token: null,
  userId: null,
  role: UserRole.Unassigned,
  login: noop,
  logout: noop,
  isAuthenticated: false
})
