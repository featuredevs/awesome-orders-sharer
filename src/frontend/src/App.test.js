import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Awesome Orders Sharer title', () => {
  render(<App />);
  const linkElement = screen.getByText(/Awesome Orders Sharer/i);
  expect(linkElement).toBeInTheDocument();
});
