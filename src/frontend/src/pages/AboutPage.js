import React from 'react'
const title = 'FeatureDevs - Awesome Orders Sharer';
const message = 'Проект "Awesome Orders Sharer" разработан в рамках учебного курса "ASP.NET Core разработчик" от компании Otus, набор - август 2020 года.'
const team = [
    'Алексей Лозинский',
    'Алексей Глущенко',
    'Абзал Танкибаев',
    'Дмитрий Передера',
    'Николай Кулеба'
]

export const AboutPage = () => {
    return(
        <div className='container'>
            <div className="row center-align">
                <div className="col s12">
                    <div className="card blue  darken-1">
                        <div className="card-content white-text">
                            <span className="card-title">{ title }</span>
                            <p>{ message }</p>
                        </div>
                        <div>
                            <ul className="collection white-text">
                                {
                                    team.map(t => {
                                        return (
                                            <li >{ t }</li>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                        <div className="card-action">
                            <a href="https://otus.ru/" target="_blank">Otus</a>
                            <a href="https://gitlab.com/featuredevs/awesome-orders-sharer" target="_blank">AOS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}