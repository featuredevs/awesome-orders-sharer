import React, { useCallback, useContext, useEffect, useState } from 'react'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import { Loader } from '../components/Loader'
import { UserProfileCard } from '../components/UserProfileCard'

// Сейчас id взят от тестового пользователя
// когда запустят Identity Server надо брать при авторизации пользователя на входе
const currentUserId = '98a70d98-f9d6-46b5-a8d9-785ca930f839'

export const UserProfilePage = () => {
    const {token} = useContext(AuthContext)
    const {request, loading} = useHttp()
    const [user, setUser] = useState(null)

    const fetchUser = useCallback(async () => {
    try {
        const fetched = await request(
        `/api/users/${currentUserId}`,
        'GET',
        null,
        {
          'Authorization': `Bearer ${token}`
        })

      setUser(fetched)
    } catch (e) {}
  }, [token, request])

  useEffect(() => {
    fetchUser()
  }, [fetchUser])

  if (loading) {
    return <Loader />
  }
    return(
        <>
            { !loading && user && <UserProfileCard user={user} /> }
        </>
    )
}