import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import {Loader} from '../components/Loader'
import {HistoryOrderList} from '../components/HistoryOrderList'

export const HistoryOrderPage = () => {
  const [orders, setOrders] = useState([])
  const {loading, request} = useHttp()
  const {token} = useContext(AuthContext)

  const fetchOrders = useCallback(async () => {
    try {
      const body = {
        "limit": 100,
        "offset": 0,
      }

      const fetched = await request(
        '/api/orders/search',
        'POST',
        body,
        {
          'Content-Type': 'application/json;charset=utf-8',
          'Authorization': `Bearer ${token}`
        }
      )

      setOrders(fetched.items)

    } catch (e) {}
  }, [token, request])

  useEffect(() => {
    fetchOrders()
  }, [fetchOrders])

  if (loading) {
    return <Loader/>
  }

  return (
    <>
      {!loading && <HistoryOrderList orders={orders} />}
    </>
  )
}