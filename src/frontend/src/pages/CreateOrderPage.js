import React, {useContext, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import {useHistory} from 'react-router-dom'

export const CreateOrderPage = () => {
  const history = useHistory()
  const auth = useContext(AuthContext)
  const {request} = useHttp()
  const [order, setOrder] = useState('')

  useEffect(() => {
    window.M.updateTextFields()
  }, [])

  const pressHandler = async event => {
    if (event.key === 'Enter') {
      try {
        const data = await request('/api/order/create', 'POST', {from: order}, {
          Authorization: `Bearer ${auth.token}`
        })
        history.push(`/detail/${data.order._id}`)
      } catch (e) {}
    }
  }

  return (
    <div className="row">
      <div className="col s8 offset-s2" style={{paddingTop: '2rem'}}>
        <div className="input-field">
          <input
            placeholder="Введите заказ"
            id="order"
            type="text"
            value={order}
            onChange={e => setOrder(e.target.value)}
            onKeyPress={pressHandler}
          />
          <label htmlFor="order">Введите заказ</label>
        </div>
      </div>
    </div>
  )
}
