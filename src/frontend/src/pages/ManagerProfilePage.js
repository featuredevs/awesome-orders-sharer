import React from 'react'

const title = 'Awesome Orders Sharer';
const subtitle = 'Профиль менеджера (тестовая страница)';

export const ManagerProfilePage = () => {
    return(
        <div className='container'>
            <header className='center-align'>
                <h1>{title}</h1>
                <h2>{subtitle}</h2>
            </header>
        </div>
    )
}