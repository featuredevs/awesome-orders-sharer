import React, { useCallback, useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import { Loader } from '../components/Loader'
import { DetailOrderCard } from '../components/DetailOrderCard'

export const DetailOrderPage = () => {
  const {token} = useContext(AuthContext)
  const {request, loading} = useHttp()
  const [order, setOrder] = useState(null)
  const orderId = useParams().id

  const fetchOrder = useCallback(async () => {
    try {
      const fetched = await request(
        `/api/orders/${orderId}`,
        'GET',
        null,
        {
          'Authorization': `Bearer ${token}`
        })

      setOrder(fetched)

    } catch (e) {}
  }, [token, orderId, request])

  useEffect(() => {
    fetchOrder()
  }, [fetchOrder])

  if (loading) {
    return <Loader />
  }

  return (
    <>
      { !loading && order && <DetailOrderCard order={order} /> }
    </>
  )
}