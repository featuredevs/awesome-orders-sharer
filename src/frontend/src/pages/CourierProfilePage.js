import React from 'react'

const title = 'Awesome Orders Sharer';
const subtitle = 'Профиль курьера (тестовая страница)';

export const CourierProfilePage = () => {
    return(
        <div className='container'>
            <header className='center-align'>
                <h1>{title}</h1>
                <h2>{subtitle}</h2>
            </header>
        </div>
    )
}