import React, {useContext, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'

export const UserProfileCard = ({ user }) => {
    const [form, setForm] = useState({
        firstName: '', lastName: '', email: '', dateOfBirth: ''
    })
    const {token} = useContext(AuthContext)
    const { loading, request } = useHttp()

    const changeHandler = event => {
        setForm({ ...form, [event.target.name]: event.target.value })
    }

    const saveHandler = async () => {
        const dateOfBirth = new Date(form.dateOfBirth).toISOString().split('T')[0].split('-').join('')
        try {
            const body = {
                "id": user.id,
                "role": user.role,
                'firstName': form.firstName,
                'lastName': form.lastName,
                'email': form.email,
                'dateOfBirth': dateOfBirth
            }
            console.log('День рождения - ', body.dateOfBirth)
            const fetched =  await request(
              `/api/users/${user.id}`,
              'PUT',
              body,
              {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
              }
            )
        } catch (e) {}
    }

    useEffect(() => {
        setForm({...user})
    }, [user]);

  return (
    <>
      <div className="col s6 offset-s3">
        <div className="card blue darken-1">
          <div className="card-content white-text">
            <span className="card-title">Профиль пользователя</span>
            <div>
                <p><strong>ID:</strong> {user.id}</p>
                <p><strong>Role:</strong> {user.role}</p>
            </div>
            <div>
                <div className="input-field">
                    <input
                        placeholder="FirstName"
                        id="firstName"
                        type="text"
                        name="firstName"
                        className="yellow-input"
                        value={form.firstName}
                        onChange={changeHandler}
                    />

                </div>
                <div className="input-field">
                    <input
                        placeholder="LastName"
                        id="lastName"
                        type="text"
                        name="lastName"
                        className="yellow-input"
                        value={form.lastName}
                        onChange={changeHandler}
                    />

                </div>
                <div className="input-field">
                    <input
                        placeholder="Введите email"
                        id="email"
                        type="text"
                        name="email"
                        className="yellow-input"
                        value={form.email}
                        onChange={changeHandler}
                    />

                </div>
              <div className="input-field">
                <input
                  placeholder="DateOfBirth"
                  id="dateOfBirth"
                  type="date"
                  name="dateOfBirth"
                  className="yellow-input"
                  value={form.dateOfBirth.substr(0, 10)}
                  onChange={changeHandler}
                />

              </div>
            </div>
          </div>
          <div className="card-action">
            <button
              className="btn yellow darken-4"
              style={{marginRight: 10}}
              disabled={loading}
              onClick={saveHandler}
            >
              Save
            </button>
          </div>
        </div>
      </div>
    </>
  )
}