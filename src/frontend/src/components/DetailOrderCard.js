import React from 'react'

export const DetailOrderCard = ({ order }) => {
  return (
    <>
      <h2>Детализация заказа</h2>

      <p><strong>ID:</strong> {order.id}</p>
      <p><strong>State:</strong> {order.state}</p>
      <p><strong>AuthorId:</strong> {order.authorId}</p>
      <p><strong>PerformerId:</strong> {order.performerId}</p>

      <p><strong>Number:</strong> {order.number}</p>
      <p><strong>Address:</strong> {order.address}</p>
      <p><strong>DeliveryDate:</strong> {order.deliveryDate}</p>

      <h3>Позиции в заказе</h3>

      <table>
      <thead>
        <tr>
          <th>№</th>
          <th>ID</th>
          <th>Description</th>
        </tr>
      </thead>

      <tbody>
        {
          order.items.map((item, index) => {
            return (
              <tr key={item.id}>
                <td>{index + 1}</td>
                <td>{item.id}</td>
                <td>{ item.description }</td>
              </tr>
            )
          })
        }
      </tbody>
    </table>
    </>
  )
}