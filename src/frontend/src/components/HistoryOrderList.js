import React from 'react'
import {Link} from 'react-router-dom'

export const HistoryOrderList = ({ orders }) => {
  if (!orders.length) {
    return <p className="center">Заказов пока нет</p>
  }

  return (
    <table>
      <thead>
      <tr>
        <th>№</th>
        <th>ID</th>
        <th>Number</th>
        <th>State</th>
        <th>DeliveryDate</th>
        <th>Detail</th>
      </tr>
      </thead>

      <tbody>
      { orders.map((order, index) => {
        return (
          <tr key={order.id}>
            <td>{index + 1}</td>
            <td>{order.id}</td>
            <td>{order.number}</td>
            <td>{order.state}</td>
            <td>{order.deliveryDate}</td>
            <td>
              <Link to={`/orders/detail/${order.id}`}><i className="material-icons">description</i></Link>
            </td>
          </tr>
        )
      }) }
      </tbody>
    </table>
  )
}
