import React, { useContext, useEffect } from 'react'
import M from 'materialize-css';
import { NavLink, useHistory } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'
import { Url } from '../data/Url'
import {UserRole} from '../data/UserRole'

export const Navbar = () => {
  const history = useHistory()
  const auth = useContext(AuthContext)
  const title = 'AOS';

  const logoutHandler = event => {
    event.preventDefault()
    auth.logout()
    history.push('/')
  }

  const profileURL = (role) => {
    return (role === UserRole.User)
      ? Url.profile.user
      : (role == UserRole.Manager)
      ? Url.profile.manager : Url.profile.courier;
  }

  useEffect( () => {
    let dropdowns = document.querySelectorAll('.dropdown-trigger');
    let options = {
        inDuration: 300,
        outDuration: 300,
        hover: true,
        coverTrigger: false,
        constrainWidth: false
    };

    M.Dropdown.init(dropdowns, options);
  })

  return (
    <nav>
      <div className="nav-wrapper blue darken-1" style={{ padding: '0 2rem' }}>
        <span className="brand-logo"><NavLink to={Url.home} >{title}</NavLink></span>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li>
            <a className="dropdown-trigger" href="#!" data-target="dropdownOrder"  style={{marginRight: 25}}><i className="material-icons">shopping_cart</i></a>
            <ul id='dropdownOrder' className='dropdown-content'>
                    <li><NavLink to={Url.create}>New Order</NavLink></li>
                    <li><NavLink to={Url.history}>History</NavLink></li>
            </ul>
          </li>
          <li>
            <a className="dropdown-trigger" href="#!" data-target="dropdownProfile" style={{marginRight: 25}}><i className="material-icons">settings</i></a>
            <ul id='dropdownProfile' className='dropdown-content'>
              <li><NavLink to={profileURL(auth.role)}>Profile</NavLink></li>
              <li className="divider" tabIndex="-1"></li>
              <li><NavLink to={Url.about}>About</NavLink></li>
            </ul>
          </li>
          <li><NavLink to={Url.auth} onClick={logoutHandler}><i className="material-icons">exit_to_app</i></NavLink></li>
        </ul>
      </div>
    </nav>
  )
}
