import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import { HistoryOrderPage } from './pages/HistoryOrderPage'
import { CreateOrderPage } from './pages/CreateOrderPage'
import { DetailOrderPage } from './pages/DetailOrderPage'
import { AuthPage } from './pages/AuthPage'
import { HomePage } from './pages/HomePage'
import { AboutPage } from './pages/AboutPage'
import { NotFoundPage } from './pages/NotFoundPage'
import { UserProfilePage } from './pages/UserProfilePage'
import { ManagerProfilePage } from './pages/ManagerProfilePage'
import { CourierProfilePage } from './pages/CourierProfilePage'

export const useRoutes = isAuthenticated => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/about" exact>
          <AboutPage />
        </Route>
        <Route path="/orders/history" exact>
          <HistoryOrderPage />
        </Route>
        <Route path="/orders/create" exact>
          <CreateOrderPage />
        </Route>
        <Route path="/orders/detail/:id">
          <DetailOrderPage />
        </Route>
        <Route path="/profile/user">
          <UserProfilePage />
        </Route>
        <Route path="/profile/manager">
          <ManagerProfilePage />
        </Route>
        <Route path="/profile/courier">
          <CourierProfilePage />
        </Route>
        <Route path="/404">
          <NotFoundPage />
        </Route>
        <Redirect to="/404" />
      </Switch>
    )
  }

  return (
    <Switch>
      <Route path="/" exact>
        <AuthPage />
      </Route>
      <Redirect to="/" />
    </Switch>
  )
}
