# This is a GitLab CI configuration to build the project as a docker image
# The file is generic enough to be dropped in a project containing a working Dockerfile
# Author: Florent CHAUVEAU <florent.chauveau@gmail.com>
# Mentioned here: https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/

# do not use "latest" here, if you want this to work in the future

image: docker:latest

services:
  - docker:dind

variables:
  PROJECT_REGISTERY: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME
  WEB_API_IMAGE: aos-web-api
  FRONTEND_IMAGE: aos-frontend
  IDENTITY_SERVICE_IMAGE: aos-identity-service
  NOTIFICATIONS_SERVICE_IMAGE: notifications-service

stages:
  - build
  - test
  - push

# Use this if your GitLab runner does not use socket binding
# services:
#   - docker:dind

###################
##  TEST TASKS   ##
###################

test-backend:
  only:
    - master
    - merge_requests
  stage: test
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  before_script:
  # run test for all solution
    - "cd src/backend"
  script:
    - dotnet test 

###################
##  BUILD TASKS  ##
###################

build-web-api:
  only:
    - master
    - merge_requests
  stage: build
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  script:
    # fetches the latest image (not failing if image is not found)
    - docker pull $PROJECT_REGISTERY/$WEB_API_IMAGE:latest || true
    # builds the project, passing proxy variables, and vcs vars for LABEL
    # notice the cache-from, which is going to use the image we just pulled locally
    # the built image is tagged locally with the commit SHA, and then pushed to 
    # the GitLab registry
    - docker build
      --pull
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --cache-from $WEB_API_IMAGE:latest
      --tag $PROJECT_REGISTERY/$WEB_API_IMAGE:$CI_COMMIT_SHA
      --file ./src/backend/Dockerfile
      ./src/backend
    - docker push $PROJECT_REGISTERY/$WEB_API_IMAGE:$CI_COMMIT_SHA

build-frontend:
  only:
    - master
    - merge_requests
  stage: build
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  script:
    # fetches the latest image (not failing if image is not found)
    - docker pull $PROJECT_REGISTERY/$FRONTEND_IMAGE:latest || true
    # builds the project, passing proxy variables, and vcs vars for LABEL
    # notice the cache-from, which is going to use the image we just pulled locally
    # the built image is tagged locally with the commit SHA, and then pushed to 
    # the GitLab registry
    - docker build
      --pull
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --cache-from $FRONTEND_IMAGE:latest
      --tag $PROJECT_REGISTERY/$FRONTEND_IMAGE:$CI_COMMIT_SHA
      --file ./src/frontend/Dockerfile
      ./src/frontend
    - docker push $PROJECT_REGISTERY/$FRONTEND_IMAGE:$CI_COMMIT_SHA

build-identity-service:
  only:
    - master
    - merge_requests
  stage: build
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  script:
    - docker pull $PROJECT_REGISTERY/$IDENTITY_SERVICE_IMAGE:latest || true
    - docker build
      --pull
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --cache-from $IDENTITY_SERVICE_IMAGE:latest
      --tag $PROJECT_REGISTERY/$IDENTITY_SERVICE_IMAGE:$CI_COMMIT_SHA
      --file ./src/identity-service/Dockerfile
      ./src/identity-service
    - docker push $PROJECT_REGISTERY/$IDENTITY_SERVICE_IMAGE:$CI_COMMIT_SHA

build-notifications-service:
  only:
    - master
    - merge_requests
  stage: build
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  script:
    - docker pull $PROJECT_REGISTERY/$NOTIFICATIONS_SERVICE_IMAGE:latest || true
    - docker build
      --pull
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --cache-from $NOTIFICATIONS_SERVICE_IMAGE:latest
      --tag $PROJECT_REGISTERY/$NOTIFICATIONS_SERVICE_IMAGE:$CI_COMMIT_SHA
      --file ./src/notifications-service/Dockerfile
      ./src/notifications-service
    - docker push $PROJECT_REGISTERY/$NOTIFICATIONS_SERVICE_IMAGE:$CI_COMMIT_SHA


###################
##  PUSH TASKS  ##
###################

# Here, the goal is to tag the "master" branch as "latest"
push_latest-backend:
  variables:
    # We are just playing with Docker here. 
    # We do not need GitLab to clone the source code.
    GIT_STRATEGY: none
  stage: push
  only:
    # Only "master" should be tagged "latest"
    - master
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  script:
    # Because we have no guarantee that this job will be picked up by the same runner 
    # that built the image in the previous step, we pull it again locally
    - docker pull $PROJECT_REGISTERY/$WEB_API_IMAGE:$CI_COMMIT_SHA
    # Then we tag it "latest"
    - docker tag $PROJECT_REGISTERY/$WEB_API_IMAGE:$CI_COMMIT_SHA $PROJECT_REGISTERY/$WEB_API_IMAGE:latest
    # Annnd we push it.
    - docker push $PROJECT_REGISTERY/$WEB_API_IMAGE:latest

push_latest-frontend:
  variables:
    GIT_STRATEGY: none
  stage: push
  only:
    - master
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  script:
    - docker pull $PROJECT_REGISTERY/$FRONTEND_IMAGE:$CI_COMMIT_SHA
    - docker tag $PROJECT_REGISTERY/$FRONTEND_IMAGE:$CI_COMMIT_SHA $PROJECT_REGISTERY/$FRONTEND_IMAGE:latest
    - docker push $PROJECT_REGISTERY/$FRONTEND_IMAGE:latest

push_latest-identity:
  variables:
    GIT_STRATEGY: none
  stage: push
  only:
    - master
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  script:
    - docker pull $PROJECT_REGISTERY/$IDENTITY_SERVICE_IMAGE:$CI_COMMIT_SHA
    - docker tag $PROJECT_REGISTERY/$IDENTITY_SERVICE_IMAGE:$CI_COMMIT_SHA $PROJECT_REGISTERY/$IDENTITY_SERVICE_IMAGE:latest
    - docker push $PROJECT_REGISTERY/$IDENTITY_SERVICE_IMAGE:latest

push_latest-identity:
  variables:
    GIT_STRATEGY: none
  stage: push
  only:
    - master
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  script:
    - docker pull $PROJECT_REGISTERY/$NOTIFICATIONS_SERVICE_IMAGE:$CI_COMMIT_SHA
    - docker tag $PROJECT_REGISTERY/$NOTIFICATIONS_SERVICE_IMAGE:$CI_COMMIT_SHA $PROJECT_REGISTERY/$NOTIFICATIONS_SERVICE_IMAGE:latest
    - docker push $PROJECT_REGISTERY/$NOTIFICATIONS_SERVICE_IMAGE:latest