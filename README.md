# <div align="center"><b>Awesome Orders Sharer</b></div>
<div align="center"><b>Учебный проект команды FeatureDevs на курсе C# ASP.NET Core разработчик в OTUS.</b></div>
<br />

**Master** [![Master pipeline status](https://gitlab.com/featuredevs/awesome-orders-sharer/badges/master/pipeline.svg)](https://gitlab.com/featuredevs/awesome-orders-sharer/-/commits/master) 

## Тема проекта
**Распределение доставок курьерам.**

## Команда
1. [Алексей Лозинский](mailto:a.lozinskyi@gmail.com)
2. [Алексей Глущенко](mailto:tuktonchik@gmail.com)
3. [Абзал Танкибаев](mailto:abzal.tankibayev@outlook.com)
4. [Дмитрий Передера](mailto:peredera@gmail.com)
5. [Николай Кулеба](mailto:n1k003@ukr.net)